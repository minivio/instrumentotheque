<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Forms\FormModification;


class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }    
    
    public function suiviModificationsAction(){
        $sm = $this->getServiceLocator();
        $form = new FormModification("formModification", $sm);
        
        return new ViewModel(array("form" => $form));
        
//        return new ViewModel();
    }
}
