<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

// indiquez au controller l'existence du formulaire
use Application\Forms\FormInstrument;

use Application\Model\Instrument;
use Application\Model\Photo;
use DateTime;

/**
 * Description of UtilisateurController
 *
 * @author Violette
 */
if(session_status() == PHP_SESSION_NONE){
    session_start();
}
class CatalogueController extends AbstractActionController {    
    
// <editor-fold defaultstate="collapsed" desc="FONCTIONS UTILES">

    public function userIsLogged(){
        return (isset($_SESSION['utilisateur']))? true : false;
    }
    
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="FAMILLES">

// =============================
// ========= FAMILLES ========== 
// =============================
    public function afficherToutesLesFamillesAction(){
        $tableFamilles = $this->getServiceLocator()->get('FamilleTableCRUD');

        $familles = $tableFamilles->obtenirFamilles();
      
        return new ViewModel(array('toutesLesFamilles'=>$familles));
    }
    
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="SOUS-FAMILLES">

// =============================
// ========= SOUS-FAMILLES ========== 
// =============================
    public function afficherToutesLesSousFamillesAction(){
        $tableSousFamilles = $this->getServiceLocator()->get('SousFamilleTableCRUD');
        $sousfamilles = $tableSousFamilles->obtenirSousFamilles();
      
        return new ViewModel(array('toutesLesSousFamilles'=>$sousfamilles));
    }
//</editor-fold>   

// <editor-fold defaultstate="collapsed" desc="CATEGORIES">

// =============================
// ========= CATEGORIES ========== 
// =============================
    public function afficherToutesLesCategoriesAction(){
        $tableCategories = $this->getServiceLocator()->get('CategorieTableCRUD');
        $categories = $tableCategories->obtenirCategories();
      
        return new ViewModel(array('toutesLesCategories'=>$categories));
    }
    
//</editor-fold>  

// <editor-fold defaultstate="collapsed" desc="MODIFICATIONS">
    
// =============================
// ========= MODIFICATIONS ========== 
// =============================

    public function afficherToutesLesModificationsAction(){
        $tableModifications = $this->getServiceLocator()->get('ModificationTableCRUD');
        $modifications = $tableModifications->obtenirModifications();
        return new ViewModel(array('toutesLesModifications'=>$modifications));
    }
    
    
//</editor-fold>   

////////////////////////////////
///// ACTIONS DEFINITIVES //////
////////////////////////////////

//<editor-fold defaultstate="collapsed" desc="PAGE PRINCIPALE">  
    public function indexAction(){
        $sm = $this->getServiceLocator();
        // "Voir catégories"
        $categoriesAvecInstruments = $sm->get("CategorieTableCRUD")->obtenirToutesLesCategoriesAvecInstruments();
        // "Recherche avancée"
        $formulaire = new FormInstrument($sm, "formInstrument"); 
        
        return new ViewModel(array("texte"=>"Données insruments",
            "form"=>$formulaire,
            "url"=>$this->getRequest()->getBaseUrl(),
            "categoriesAvecInstruments"=> $categoriesAvecInstruments
        ));        
    }
    
// permet de peupler la liste des catégories en fonction de la sous-famille sélectionnée
    public function rechercheCategoriesAjaxAction(){
        // id de la sous-famille sélectionnée
        $request = $this->getRequest();
        $id_ssFamille = $request->getPost('id_sousfamille');
        
        // catégories de cette sous-famille
        $tableCategories = $this->getServiceLocator()->get('CategorieTableCRUD');
        $categories = $tableCategories->obtenirCategoriesAvecFiltre(['id_sousfamille' => $id_ssFamille]);
        $categoriesArray = [];
        foreach($categories as $objCat){
            $categoriesArray[] = $objCat->toArray();
        }
        
        $response = $this->getResponse();
        $response->setContent(json_encode(['categories' => $categoriesArray]));
        
        return $response;
    }
    
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="RECHERCHE AVANCEE - RESULTATS DE RECHERCHE">  

    public function resultatRechercheInstrumentAjaxAction(){
        // on récupère chacune des données récupérées en post via le call ajax
        $id_sousfamille = $this->getRequest()->getPost('id_sousfamille');
        $id_categorie = $this->getRequest()->getPost('id_categorie');
        $ref_mc = $this->getRequest()->getPost('ref_mc');
        $emprunt_ok = $this->getRequest()->getPost('emprunt_ok');
        
        // on va chercher les instruments requis dans la bdd
        $instrumentsTable = $this->getServiceLocator()->get("InstrumentTableCRUD");
        if($ref_mc != null){
            $recherche = $ref_mc; // une recherche par référence est prioritaire et annule toutes les autres (sauf option emprunt possible)
            $listeInstruments = $instrumentsTable->obtenirInstrumentsAvecFiltre(['ref_mc'=>$ref_mc]);
        }else if($id_categorie != null){ // recherche par catégorie
            $listeInstrumentsAvecCategorie = $instrumentsTable->obtenirInstrumentsParIdCategorieAvecNomCategorie($id_categorie, $emprunt_ok);
            $listeInstruments = $listeInstrumentsAvecCategorie['listeInstruments'];
            $recherche = $listeInstrumentsAvecCategorie['nomCategorie'];
        }else if($id_sousfamille != null){ // recherche par sous-famille (si catégorie pas sélectionnée)
            $listeInstrumentsAvecSousfamille = $instrumentsTable->obtenirInstrumentsParIdSousfamilleAvecNomSousfamille($id_sousfamille, $emprunt_ok);
            $listeInstruments = $listeInstrumentsAvecSousfamille['listeInstruments'];            
            $recherche = $listeInstrumentsAvecSousfamille['nomSousfamille'];
        }else{ // si on n'a rien complété, on affiche tous les instruments (éventuellement filtrés si option emprunt)
            $recherche = "TOUS";
            $listeInstruments = ($emprunt_ok == 1)? $instrumentsTable->obtenirInstrumentsAvecFiltre(['emprunt_ok'=>$emprunt_ok]): $instrumentsTable->obtenirInstruments();
        }
        // on envoie les données à une vue qui génère le html dont on a besoin, qu'on revnvoie ensuite vers notre ajax qui se charge de l'insérer da,s
        $view = new ViewModel(['instruments' => $listeInstruments, 'recherche' => $recherche, 'option' => $emprunt_ok, 'nbResultats' => count($listeInstruments)]);
        $view->setTerminal(true)->setTemplate('application/catalogue/partial/resultat-recherche-instruments.phtml');
        // on renvoie le html généré vers notre ajax, qui se charge de l'insérer dans le DOM
        return $view;
    }
    
    //<editor-fold defaultstate="collapsed" desc="ESSAI DE REFACTORISATION DU CODE - en cours">  
//    public function determinerRecherche($request){
//        $typeRecherche = "";
//        if($request->getPost('ref_mc') != null){ // une recherche par référence est prioritaire et annule toutes les autres
//            $typeRecherche = 'ref_mc'; 
//            $termeRecherche = $request->getPost('ref_mc');
//        }else if($request->getPost('id_categorie') != null){ // recherche par catégorie
//            $typeRecherche = 'id_categorie'; 
//            $termeRecherche = $request->getPost('id_categorie');        
//        }else if($request->getPost('id_sousfamille') != null){ // recherche par sous-famille (si catégorie pas sélectionnée)
//            $typeRecherche = 'id_sousfamille'; 
//            $termeRecherche = $request->getPost('id_sousfamille');      
//        }else{ // si on n'a rien complété, on recherche tous les instruments (éventuellement filtrés si option emprunt)
//            $termeRecherche = 'tous'; 
//        }
//        return ['typeRecherche' => $typeRecherche, 'termeRecherche' => $termeRecherche, 'emprunt_ok' => $request->getPost('emprunt_ok')];        
//    }
//    
//    public function recupererListeInstrumentRecherches($request){
//        $recherche = determinerRecherche($request);
//        $termeRecherche = $recherche['termeRecherche'];
//        $optionEmprunt = $recherche['emprunt_ok'];
//        
//        $instrumentsTable = $this->getServiceLocator()->get("InstrumentTableCRUD");
//        switch ($recherche['typeRecherche']){
//            case 'ref_mc':
//                $listeInstruments = $instrumentsTable->obtenirInstrumentsAvecFiltre(['ref_mc'=>$termeRecherche]); break;
//            case 'id_categorie':
//                $listeInstrumentsAvecCategorie = $instrumentsTable->obtenirInstrumentsParIdCategorieAvecNomCategorie($termeRecherche, $optionEmprunt);
//                $listeInstruments = $listeInstrumentsAvecCategorie['listeInstruments'];
//                $termeRecherche = $listeInstrumentsAvecCategorie['nomCategorie']; break;
//            case 'id_sous-famille':
//                $listeInstrumentsAvecSousfamille = $instrumentsTable->obtenirInstrumentsParIdSousfamilleAvecNomSousfamille($termeRecherche, $optionEmprunt);
//                $listeInstruments = $listeInstrumentsAvecSousfamille['listeInstruments'];            
//                $termeRecherche = $listeInstrumentsAvecSousfamille['nomSousfamille']; break;
//            default:
//                $listeInstruments = ($optionEmprunt == 1)? $instrumentsTable->obtenirInstrumentsAvecFiltre(['emprunt_ok'=>$optionEmprunt]): $instrumentsTable->obtenirInstruments(); break;
//        }
//        return ['listeInstruments' => $listeInstruments, 'termeRecherche' => $termeRecherche, 'optionEmprunt' => $optionEmprunt];
//    }
//    public function resultatRechercheInstrumentAjaxAction(){
//        $request = $this->getRequest();
//        $resultatRecherche = recupererListeInstrumentRecherches($request);
//        $listeInstruments = $resultatRecherche['listeInstruments'];
//        // on envoie les données à une vue qui génère le html dont on a besoin, qu'on revnvoie ensuite vers notre ajax qui se charge de l'insérer da,s
//        $view = new ViewModel(['instruments' => $listeInstruments, 'recherche' => $resultatRecherche['termeRecherche'], 'option' => $resultatRecherche['optionEmprunt'], 'nbResultats' => count($listeInstruments)]);
//        $view->setTerminal(true);
//        $view->setTemplate('layout/resultat-recherche-instruments.phtml');
//        // on renvoie le html généré vers notre ajax, qui se charge de l'insérer dans le DOM
//        return $view;
//    }
    //</editor-fold>

//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="MAXI FICHE INSTRUMENT">
    
    public function afficherInstrumentParIdAction(){
        $valID= $this->getRequest()->getPost('id');
        $instrumentsTable= $this->getServiceLocator()->get("InstrumentTableCRUD");
        $objInstrument = $instrumentsTable->obtenirInstrumentParId($valID);//obtenir les données de la BD
        
        $categorieTable= $this->getServiceLocator()->get("CategorieTableCRUD");
        $objCategorie = $categorieTable->obtenirCategorieParId($objInstrument->getId_categorie());//obtenir les données de la BD

        if($objInstrument->getInfos_comp1() === "<p><br></p>" || $objInstrument->getInfos_comp1() == null){
            $objInstrument->setInfos_comp1($objCategorie->getInfos_comp1());
        }
        if($objInstrument->getInfos_comp2() === "<p><br></p>" || $objInstrument->getInfos_comp2() == null){
            $objInstrument->setInfos_comp2($objCategorie->getInfos_comp2());
        }
        if($objInstrument->getVideo()== null){
            $objInstrument->setVideo($objCategorie->getVideo());
        }
        
        
        $view = new ViewModel(['instrument' => $objInstrument]);
        $view->setTerminal(true)->setTemplate('application/catalogue/partial/maxi-fiche-instrument.phtml');
        return $view;
    }
//</editor-fold>
   
//<editor-fold defaultstate="collapsed" desc="INSERTION D'UN NOUVEL INSTRUMENT">

    public function insertInstrumentAction(){
        $instrumentTable= $this->getServiceLocator()->get("InstrumentTableCRUD");
        if($instrumentTable->refMcDisponible($this->getRequest()->getPost('ref_mc'))){
            
            $instrumentFormulaire = $this->getRequest()->getPost();
            $instrument = new Instrument((array)$instrumentFormulaire);
            $resultatInsertion = $instrumentTable->insertInstrument($instrument);
            
            $modifTable = $this->getServiceLocator()->get('ModificationTableCRUD');
            $idInstrument = $instrumentTable->getIdLastInserted();
            $modifTable->setModifInsertInstrument($idInstrument);
        }
        else{
            $resultatInsertion = false;
        }
        return ($resultatInsertion)? true : false;
    }
    
    public function insererPhotosNouvelInstrumentAction(){
        if(!empty($_FILES)){
            $tableInstruments = $this->getServiceLocator()->get('InstrumentTableCRUD');
            $idInstrument = $tableInstruments->getIdLastInserted();
            $targetPath = 'public/img/instruments/';
            for($i = 0; $i < count($_FILES['file']['name']); $i++){
                $now = new DateTime();
                $timestamp = $now->getTimestamp();
                $tempFile = $_FILES['file']['tmp_name'][$i];
                $targetFile = basename($timestamp.$_FILES['file']['name'][$i]);
                if(move_uploaded_file($tempFile, $targetPath.$targetFile)){
                    $photoTable = $this->getServiceLocator()->get('PhotoTableCRUD');
                    $objPhoto = new Photo (['nom'=>$targetFile, 'id_instrument'=>$idInstrument]);
                    $photoTable->insererPhoto($objPhoto);
                }
            }
            return true;
        }
        return false;
    }
    
    
    public function determinerNumExemplaireAction(){
        $id_categorie = $this->getRequest()->getPost('id_categorie');
        $instrumentTable = $this->getServiceLocator()->get('InstrumentTableCRUD');
        $numero = ($instrumentTable->obtenirNumeroInstrumentParIdCategorie($id_categorie));
        $response = $this->getResponse();
        $response->setContent($numero);
        return $response;
    }
    
    public function verifierRefMcAction(){
        $ref_mc = $this->getRequest()->getPost('ref_mc');
        $instrumentTable = $this->getServiceLocator()->get('InstrumentTableCRUD');
        $refDispo = $instrumentTable->refMcDisponible($ref_mc);
        
        $response = $this->getResponse();
        $response->setContent($refDispo);
        return $response;
    }
    
 //</editor-fold> 

//<editor-fold defaultstate="collapsed" desc="UPDATE INSTRUMENT">
 
    public function afficherFormUpdateInstrumentAvecDropzoneAction(){
        $idInstrument = $this->getRequest()->getPost('id');
        $sm = $this->getServiceLocator();
        $formulairePourListeCategories = new FormInstrument($sm, "formInstrument");
        $instrumentsTable= $sm->get("InstrumentTableCRUD");
        $objInstrument = $instrumentsTable->obtenirInstrumentParId($idInstrument);
        $view = new ViewModel(['instrumentAvecPhotos' => $objInstrument, 'form' => $formulairePourListeCategories]);
        $view->setTerminal(true);
        $view->setTemplate('application/catalogue/partial/form-update-instrument-avec-dropzone.phtml');
        return $view;
    }
    
    public function updateInstrumentAction(){
        $arrayUpdate = [];
        foreach($this->getRequest()->getPost() as $key => $value){
                $arrayUpdate[$key] = $value;
        }
        $updatedInstrument = new Instrument($arrayUpdate);
        $instrumentTable = $this->getServiceLocator()->get("InstrumentTableCRUD");
        if($instrumentTable->updateInstrument($updatedInstrument)){
            $msg = "L'instrument a bien été modifié!";
            $modifTable = $this->getServiceLocator()->get('ModificationTableCRUD');
            $idInstrument = $updatedInstrument->getId();
            $modifTable->setModifUpdateInstrument($idInstrument);
        }
        else{
            $msg = "Erreur";
        }
        $response = $this->getResponse();
        $response->setContent(true);
        return $response;
    }
        
    public function insererPhotosInstrumentUpdateAction(){
        if(!empty($_FILES)){
            $idInstrument = $_POST['idInstrument'];
            $targetPath = 'public/img/instruments/';
            for($i = 0; $i < count($_FILES['file']['name']); $i++){
                $now = new DateTime();
                $timestamp = $now->getTimestamp();
                $tempFile = $_FILES['file']['tmp_name'][$i];
                $targetFile = basename($timestamp.$_FILES['file']['name'][$i]);
                if(move_uploaded_file($tempFile, $targetPath.$targetFile)){
                    $photoTable = $this->getServiceLocator()->get('PhotoTableCRUD');
                    $objPhoto = new Photo (['nom'=>$targetFile, 'id_instrument'=>$idInstrument]);
                    $photoTable->insererPhoto($objPhoto);
                }
            }
            return true;
        }
        return false;
    }
//</editor-fold>
 
//<editor-fold defaultstate="collapsed" desc="DELETE INSTRUMENT">
public function deleteInstrumentAction(){
    $idInstru = $this->getRequest()->getPost('id');
    // on supprime d'abord les photos liées à cet instrument (contrainte de clé étrangère dans la bdd)
    $photoTable = $this->getServiceLocator()->get("PhotoTableCRUD");
    $photoTable->deletePhotosParIdInstrument(['id_instrument' => $idInstru]);
    // on récupère l'instrument qu'on veut supprimer
    $instrumentTable = $this->getServiceLocator()->get("InstrumentTableCRUD");
    $instrumentObj = $instrumentTable->obtenirInstrumentParId($idInstru);
    
    // on injecte tottes les infos relatives à l'instrument qu'on va supprimer dans la description de la modification
    $modifTable = $this->getServiceLocator()->get('ModificationTableCRUD');
    $description = "";
    foreach($instrumentObj->toArray() as $key => $value){
        $description .= "--".$key."--".$value."<br/>";
    }
    $modifTable->setModifDeleteInstrument($description);
            
    // on supprime ensuite l'instrument
    $msg = "TEST";
    if($instrumentObj == null){
        $msg = "Aucun instrument ne correspond à la recherche";
    }
    else if($instrumentTable->deleteInstrument($instrumentObj)){
        $msg = "Suppression ok";
        
    }
    else {
        $msg="Probleme supression";
    }
    $response = $this->getResponse();
    $response->setContent($msg);
    return $response;
}
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="FONCTIONS TEST A SUPPRIMER">

    public function testLastInstrumentAction(){
        $tableInstruments = $this->getServiceLocator()->get('InstrumentTableCRUD');
        $msg = $tableInstruments->getIdLastInserted();
        echo "LAST = ".$msg;
        var_dump($tableInstruments->getIdLastInserted());
        return new ViewModel(['msg' => $msg]);
    }
    
    public function formTestReferenceAction(){
        return New ViewModel();
    }
    
    public function referenceTestAction(){
        // se des fichiers avec le name 'photo' on été envoyés,
        if(isset($_FILES['photo'])){     
            // on récupère l'ID du dernier instrument inséré dans la bdd, afin de pouvoir le mettre en clé étrangère des photos
            $instrumentTable = $this->getServiceLocator()->get('InstrumentTableCRUD');
            $idInstrument = $instrumentTable->getIdLastInserted();
            
            // on récupère la date actuelle en vue de concaténer un timestamp au nom original de chaque fichier reçu
            $now = new DateTime();
            $now->format('Y-m-d H:i:s');
            // on cible le dossier où stocker la photo
            $cheminDossier = 'public/img/instruments/';
            $ref = "";
            // la table photo permettra d'insérer les photos dans la bdd
            $photoTable = $this->getServiceLocator()->get("PhotoTableCRUD");   
            
            // pour chaque fichier reçu,
            for ($i = 0; $i < count($_FILES['photo']['name']); $i++) {
                $fichierEnvoye = $_FILES['photo']['name'][$i]; // nom original
                $timestamp = $now->getTimestamp(); // timestamp
                $nomFichierFinal = basename($timestamp.$fichierEnvoye); // nom de stockage
                
                // si le fichier (avec son nom temporaire) a bien été déplacé dans le bon dossier (avec son nouveau nom)
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $cheminDossier.$nomFichierFinal)){
                    // on insère un nouvel objet Photo dans la bdd
                    $objPhoto = new Photo (['nom'=>$nomFichierFinal, 'id_instrument'=>$idInstrument]);
                    $photoTable->insererPhoto($objPhoto);
                }
            }
        }
        
        return new ViewModel(['ref'=> $ref]);
    }
 //</editor-fold>   


}