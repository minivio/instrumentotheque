<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Utilisateur;
// indiquez au controller l'existence du formulaire
use Application\Forms\FormUtilisateur;

/**
 * Description of UtilisateurController
 *
 * @author Violette
 */
if(session_status() == PHP_SESSION_NONE){
    session_start();
}

class UtilisateurController extends AbstractActionController {
    
/* =================================
   ===== FONCTIONS UTILITAIRES =====
   ================================= */

    public function userIsLogged(){
        return (isset($_SESSION['utilisateur']))? true : false;
    }
    
    public function adminIsLogged(){
        return (isset($_SESSION['utilisateur']) && $_SESSION['utilisateur']['id_role'] == 1)? true : false;
    }
    
    public function getUserLogin(){
            return ($this->userIsLogged())? $_SESSION['utilisateur']['login'] : "";
    }
    
    public function hashPassword($mdp){
        $newMdp = password_hash($mdp, PASSWORD_DEFAULT, ['cost'=>12]);
        return $newMdp;
    }
    
    public function getLoggedUserId(){
        return ($this->userIsLogged())? $_SESSION['utilisateur']['id'] : "";
    }
/* ================================================
   ===== PAGE PRINCIPALE & ACTIONS RELATIVES  =====
   ================================================ */

    public function indexAction(){
        
        if($this->adminIsLogged()){
            $sm = $this->getServiceLocator();
            $form = new FormUtilisateur("formUtilisateur", $sm);
            return new ViewModel(["form"=>$form, "url"=>$this->getRequest()->getBaseUrl()]);
        }
        else{
            $noaccess = "Accès réservé aux administrateurs.";
            return new ViewModel(['noaccess'=>$noaccess]);
        }
        
        return new ViewModel();
    }
    

    public function afficherTousLesUtilisateursAction(){
        $tableUtilisateurs = $this->getServiceLocator()->get('UtilisateurTableCRUD');
        $utilisateurs = $tableUtilisateurs->obtenirUtilisateurs();
        return new ViewModel(array('tousLesUtilisateurs'=>$utilisateurs));
    }
    
    // fonction de traitement des données de formulaire d'ajout d'utilisateur reçues via un call ajax
    public function ajouterUtilisateurAction(){
        $request = $this->getRequest();
        $messageResultat = $this->verificationsAjout($request);
        if($messageResultat == ""){
            $messageResultat = $this->insertBDD($request);
        }
        $response = $this->getResponse();
        $response->setContent($messageResultat);
        return $response;
    }
    
    public function verificationsAjout($request){
        $mdp = $request->getPost('mdp');
        $mdp_check = $request->getPost('mdp_check');
        $email = $request->getPost('email');
        $email_check = $request->getPost('email_check');
        
        $messageResultat = ($mdp != $mdp_check)? "Vérifiez le mot de passe.<br/>":"";
        $messageResultat .= ($email != $email_check) ? "Vérifiez l'adresse email.<br/>" : "";
        
        $login = $request->getPost('login');
        $messageResultat .= ($this->loginDispoAction($login))? "": "Veuillez choisir un autre login svp!<br/>";
        return $messageResultat;
    }
    
    // vérifie si le login choisi est déjà pris (lorsqu'on complète le formulaire, mais aussi juste avant insertion dans la bdd)
    public function loginDispoAction(){
        $login = $this->getRequest()->getPost('login');
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $resultat = $utilisateurTable->obtenirUtilisateurParLogin($login);
        
        $response = $this->getResponse();
        $response->setContent(($resultat)? false : true);
        return $response;
    }

    public function insertBDD($request){
        $donneesFormulaire = $request->getPost();
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $newUtilisateur = new Utilisateur((array)$donneesFormulaire);
        if($request->getPost('mdp') != ""){
            $newUtilisateur->setMdp($this->hashPassword($request->getPost('mdp')));
        }
        $resultat = $utilisateurTable->insertUtilisateur($newUtilisateur);
        $id = $utilisateurTable->getIdLastInsertedUser();
        $tableModif = $this->getServiceLocator()->get('ModificationTableCRUD');
        $tableModif->setModifInsertUtilisateur($id);
        return ($resultat)? "Nouvel utilisateur correctement inséré." : "Problème d'insertion.";
    }

// <editor-fold defaultstate="collapsed" desc="LOGIN & LOGOUT">

/* ===========================
   ===== LOGIN & LOGOUT  =====
   =========================== */
    
    // fonction de traitement des données reçues depuis le formulaire de login (call ajax)
    public function loginFormResultAction(){
        $login = $this->getRequest()->getPost('login');
        $mdpForm = $this->getRequest()->getPost('mdp');
        $utilisateurTable= $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $utilisateurObj = $utilisateurTable->obtenirUtilisateurParLogin($login);
        $msg = "";
        
        if($utilisateurObj != null && password_verify($mdpForm, $utilisateurObj->getMdp() )){
            $_SESSION['utilisateur'] = $utilisateurObj->toArray();            
            $msg = $login;
        }
        
        $response = $this->getResponse();
        $response->setContent($msg);

        return $response;    
    }
    
    // fonction de traitement d'une demande de déconnexion (call ajax)
    public function logoutAction(){
        if($this->userIsLogged()){
            session_destroy();
            $msg = "vous avez été déconnecté."; 
        }
        else{
            $msg = "vous étiez déjà déconnecté";
        }
        return $msg;
    }
//</editor-fold>
    
    public function rechercheUtilisateurAction(){
        // on récupère chacune des données récupérées en post via le call ajax
        $arrayFiltres = [];
        $recherche = "";
        foreach($this->getRequest()->getPost() as $key => $value){
            if($value != "" && $value !== "false"){
                if($key === "newsletter"){
                    $value = ($value === "true")? 1 : "";
                    $recherche .= " [".$key." : oui] ";
                }else{
                    $recherche .= " [".$key." : ".$value."] "; 
                }
                $arrayFiltres[$key] = $value;
            }
        }
        if(count($arrayFiltres) == 0){ $recherche = "TOUS"; }
        
        // on va chercher les utilisateurs requis dans la bdd
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $listeUtilisateurs = $utilisateurTable->obtenirUtilisateurAvecFiltre($arrayFiltres);

        // on envoie les données à une vue qui génère le html dont on a besoin
        $view = new ViewModel(['listeUtilisateurs' => $listeUtilisateurs, 'recherche' => $recherche, 'nbResultats' => count($listeUtilisateurs)]);
        $view->setTerminal(true);
        $view->setTemplate('application/utilisateur/partial/liste-fiches-utilisateurs.phtml');
        // on renvoie le html généré vers notre ajax, qui se charge de l'insérer dans le DOM
        return $view;
    }
    
    public function getEmailsNewsletterAction(){
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $listeEmails = $utilisateurTable->obtenirEmailsNewsletter();
        $stringEmails = "";
        foreach($listeEmails as $objUtilisateur){
            $email = $objUtilisateur->getEmail();
            $stringEmails .= ($email != "")? $email."; " : "";
        }        
        $response = $this->getResponse();
        $response->setContent($stringEmails);
        return $response;
    }
    
    public function profilPersonnelAction(){
        $id = $this->getLoggedUserId();
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        $objUtilisateur = $utilisateurTable->obtenirUtilisateurParId($id);
        
        $view = new ViewModel(['utilisateur' => $objUtilisateur]);
        $view->setTerminal(true);
        $view->setTemplate('application/utilisateur/partial/fiche-utilisateur.phtml');
        return $view;
    }
    
    public function afficherFormUpdateUtilisateurAction(){
        $idUtilisateur = $this->getRequest()->getPost('id');
        $sm = $this->getServiceLocator();
        $form = new FormUtilisateur("formUtilisateur", $sm);
        $utilisateursTable= $sm->get("UtilisateurTableCRUD");
        $objUtilisateur = $utilisateursTable->obtenirUtilisateurParId($idUtilisateur);
        
        $tableRoles = $sm->get("RoleTableCRUD");
        $nomRole = $tableRoles->obtenirIntituleRoleParId($objUtilisateur->getId_role());
        
        $view = new ViewModel(['utilisateur' => $objUtilisateur, 'form' => $form, 'nomRole' => $nomRole]);
        $view->setTerminal(true);
        $view->setTemplate('application/utilisateur/partial/form-update-utilisateur.phtml');
        return $view;
    }
    
    public function updateUtilisateurAction(){
        // on récupère les données envoyées en post via ajax
        $arrayUpdate = [];
        foreach($this->getRequest()->getPost() as $key => $value){
                $arrayUpdate[$key] = $value;
        }
        // on crée un nouvel objet utilisateur à patir de ces données
        $updatedUser = new Utilisateur($arrayUpdate);
        
        $utilisateurTable = $this->getServiceLocator()->get("UtilisateurTableCRUD");
        // si c'est un utilisateur de rôle 1 ou 2
        if($updatedUser->getId_role() < 3){
            switch($updatedUser->getMdp()){
                // si on n'a pas entré de nouveau mot de passe
                case "": 
                    // on récupère le mot de passe d'origine
                    $originalUser = $utilisateurTable->obtenirUtilisateurParId($updatedUser->getId());
                    $updatedUser->setMdp($originalUser->getMdp()); break;
                // si on a entré un nouveau mot de passe
                default:
                    // on hache le nouveau mot de passe
                    $updatedUser->setMdp($this->hashPassword($this->getRequest()->getPost('mdp'))); break;
            }
        }
        $tableModif = $this->getServiceLocator()->get("ModificationTableCRUD");
        $tableModif->setModifUpdateUtilisateur($updatedUser->getId());
        // on renvoie le résultat de la requête update
        $response = $this->getResponse();
        $response->setContent($utilisateurTable->updateUtilisateur($updatedUser));
        return $response;

    }
}
