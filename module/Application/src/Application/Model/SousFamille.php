<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SousFamille
 *
 * @author Violette
 */
class SousFamille {
    //put your code here
    private $id;
    private $legende;
    private $id_famille;
    private $nom;
    private $description;
    private $listeInstruments = [];
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setLegende(!empty($data['legende'])?$data['legende']:null);
        $this->setId_famille(!empty($data['id_famille'])?$data['id_famille']:null);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
        $this->setDescription(!empty($data['description'])?$data['description']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    function getId() {
        return $this->id;
    }
    
    function getLegende() {
        return $this->legende;
    }
    
    function getId_famille() {
        return $this->id_famille;
    }

    function getNom() {
        return $this->nom;
    }

    function getDescription() {
        return $this->description;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setLegende($legende) {
        $this->legende = $legende;
    }

    function setId_famille($id_famille) {
        $this->id_famille = $id_famille;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    function getListeCategories() {
        return $this->listeCategories;
    }

    function getListeInstruments() {
        return $this->listeInstruments;
    }

    function setListeInstruments($listeInstruments) {
        $this->listeInstruments = $listeInstruments;
    }

        
    function toArray(){
        return get_object_vars($this);
    }

    
}
