<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class FamilleTable {
    //put your code here
    public $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function obtenirFamilles(){
        return($this->tableGateway->select()); 
    }
    
    public function obtenirFamillesAvecFiltre($filtre){
        return ($this->tableGateway->select($filtre));
    }
}
