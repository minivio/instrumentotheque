<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Instrument
 *
 * @author Violette
 */
class Instrument {
    private $id;
    private $id_categorie;
    private $ref_mc;
    private $ref_mf;
    private $nom;
    private $detail_description;
    private $accessoires;
    private $emprunt_ok;
    private $infos_comp1;
    private $infos_comp2;
    private $video;
    private $listePhotos = [];
    private $nomSousfamille;
    private $nomCategorie;
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setId_categorie(!empty($data['id_categorie'])?$data['id_categorie']:null);
        $this->setRef_mc(!empty($data['ref_mc'])?$data['ref_mc']:null);
        $this->setRef_mf(!empty($data['ref_mf'])?$data['ref_mf']:null);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
        $this->setDetail_description(!empty($data['detail_description'])?$data['detail_description']:null);
        $this->setAccessoires(!empty($data['accessoires'])?$data['accessoires']:null);
        $this->setEmprunt_ok(!empty($data['emprunt_ok'])?$data['emprunt_ok']:null);
        $this->setInfos_comp1(!empty($data['infos_comp1'])?$data['infos_comp1']:null);
        $this->setInfos_comp2(!empty($data['infos_comp2'])?$data['infos_comp2']:null);
        $this->setVideo(!empty($data['video'])?$data['video']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
    
    function getId() {
        return $this->id;
    }

    function getId_categorie() {
        return $this->id_categorie;
    }

    function getRef_mc() {
        return $this->ref_mc;
    }

    function getRef_mf() {
        return $this->ref_mf;
    }

    function getNom() {
        return $this->nom;
    }

    function getEmprunt_ok() {
        return $this->emprunt_ok;
    }

    function getInfos_comp1() {
        return $this->infos_comp1;
    }

    function getInfos_comp2() {
        return $this->infos_comp2;
    }

    function getVideo() {
        return $this->video;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_categorie($id_categorie) {
        $this->id_categorie = $id_categorie;
    }

    function setRef_mc($ref_mc) {
        $this->ref_mc = $ref_mc;
    }

    function setRef_mf($ref_mf) {
        $this->ref_mf = $ref_mf;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setEmprunt_ok($emprunt_ok) {
        $this->emprunt_ok = $emprunt_ok;
    }

    function setInfos_comp1($infos_comp1) {
        $this->infos_comp1 = $infos_comp1;
    }

    function setInfos_comp2($infos_comp2) {
        $this->infos_comp2 = $infos_comp2;
    }

    function setVideo($video) {
        $this->video = $video;
    }

    function getDetail_description() {
        return $this->detail_description;
    }

    function getAccessoires() {
        return $this->accessoires;
    }

    function setDetail_description($detail_description) {
        $this->detail_description = $detail_description;
    }

    function setAccessoires($accessoires) {
        $this->accessoires = $accessoires;
    }
    
    function getListePhotos() {
        return $this->listePhotos;
    }

    function setListePhotos($listePhotos) {
        $this->listePhotos = $listePhotos;
    }
    
    function getNomSousfamille() {
        return $this->nomSousfamille;
    }

    function setNomSousfamille($nomSousfamille) {
        $this->nomSousfamille = $nomSousfamille;
    }
    
    function getNomCategorie() {
        return $this->nomCategorie;
    }

    function setNomCategorie($nomCategorie) {
        $this->nomCategorie = $nomCategorie;
    }

    
        
    function toArray(){
        $instrumentArray = get_object_vars($this);
        unset($instrumentArray['listePhotos']);
        unset($instrumentArray['nomSousfamille']);
        unset($instrumentArray['nomCategorie']);
        return $instrumentArray;
    }
    
}
