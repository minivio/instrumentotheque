<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Famille
 *
 * @author Violette
 */
class Famille {
    //put your code here
    private $id;
    private $legende;
    private $nom;
    private $description;
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setLegende(!empty($data['legende'])?$data['legende']:null);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
        $this->setDescription(!empty($data['description'])?$data['description']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    
    function getId() {
        return $this->id;
    }
    
    function getLegende() {
        return $this->legende;
    }
    
    function getNom() {
        return $this->nom;
    }

    function getDescription() {
        return $this->description;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setLegende($legende) {
        $this->legende = $legende;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    function toArray(){
        return get_object_vars($this);
    }


}
