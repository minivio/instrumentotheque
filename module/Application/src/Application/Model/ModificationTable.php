<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class ModificationTable {
    //put your code here
    public $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function obtenirModifications(){
        return($this->tableGateway->select()); 
    }
    
//<editor-fold defaultstate="collapsed" desc="CREATION ET INSERTION D'UNE MODIFICATION">

    public function insertModif($newModif){
        return $this->tableGateway->insert($newModif->toArray());
    }
    
    public function createNewModif(){
        $newModif  = new Modification();
        $newModif->setId_loggedUser($this->getLoggedUserId());
        return $newModif;
    }
    public function getLoggedUserId(){
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }
        return $_SESSION['utilisateur']['id'];
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="MODIFICATIONS SUR UN INSTRUMENT">

    public function setModifInsertInstrument($id_instrument){
        $newModif  = $this->createNewModif();
        $newModif->setId_instrument($id_instrument);
        $newModif->setType_modif("insert");
        return $this->insertModif($newModif);
    }
    
    public function setModifUpdateInstrument($id_instrument){
        $newModif  = $this->createNewModif();
        $newModif->setId_instrument($id_instrument);
        $newModif->setType_modif("update");
        return $this->insertModif($newModif);
    }
    
    public function setModifDeleteInstrument($description){
        $newModif  = $this->createNewModif();
        $newModif->setType_modif("delete");
        $newModif->setDescription($description);
        return $this->insertModif($newModif);
    }
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc="MODIFICATIONS SUR UN UTILISATEUR">

    public function setModifInsertUtilisateur($id_utilisateur){
        $newModif  = $this->createNewModif();
        $newModif->setId_utilisateur($id_utilisateur);
        $newModif->setType_modif("insert");
        return $this->insertModif($newModif);
    }
    
    public function setModifUpdateUtilisateur($id_utilisateur){
        $newModif  = $this->createNewModif();
        $newModif->setId_utilisateur($id_utilisateur);
        $newModif->setType_modif("update");
        return $this->insertModif($newModif);
    }
//</editor-fold> 
    
    
}
