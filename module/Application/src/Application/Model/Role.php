<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role
 *
 * @author Violette
 */
class Role {
    //put your code here
    private $id;
    private $intitule;
    
     function toArray(){
        return get_object_vars($this);
    }

    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setIntitule(!empty($data['intitule'])?$data['intitule']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    
    function getId() {
        return $this->id;
    }

    function getIntitule() {
        return $this->intitule;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setIntitule($intitule) {
        $this->intitule = $intitule;
    }

}
