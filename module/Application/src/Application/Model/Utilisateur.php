<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Utilisateur
 *
 * @author Violette
 */
class Utilisateur {
    //put your code here
    private $id;
    private $id_role;
    private $nom;
    private $email;
    private $tel;
    private $newsletter;
    private $login;
    private $mdp;
    
    // cette méthode joue le rôle de constructeur. C'est une sorte d'hydratation.
    // Elle doit porter ce nom car elle est utilisée par la classe TableGateway 
    // TableGateway = classe fournie par Zend2 qui simplifie l'exécution de requêtes sur la bdd
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setId_role(!empty($data['id_role'])?$data['id_role']:3);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
        $this->setEmail(!empty($data['email'])?$data['email']:null);
        $this->setTel(!empty($data['tel'])?$data['tel']:null);
        $this->setNewsletter(!empty($data['newsletter'])?$data['newsletter']:null);
        $this->setLogin(!empty($data['login'])?$data['login']:null);
        $this->setMdp(!empty($data['mdp'])?$data['mdp']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    function getId() {
        return $this->id;
    }

    function getId_role() {
        return $this->id_role;
    }

    function getNom() {
        return $this->nom;
    }

    function getEmail() {
        return $this->email;
    }

    function getTel() {
        return $this->tel;
    }

    function getNewsletter() {
        return $this->newsletter;
    }

    function getLogin() {
        return $this->login;
    }

    function getMdp() {
        return $this->mdp;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_role($id_role) {
        $this->id_role = $id_role;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setTel($tel) {
        $this->tel = $tel;
    }

    function setNewsletter($newsletter) {
        // lors de la création d'un nouvel utilisateur, la valeur transmise via ajax est transformée en string.
        // json_decode permet de convertir cette valeur en booleen
        $this->newsletter = json_decode($newsletter);
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setMdp($mdp) {
        $this->mdp = $mdp;
    }
    
    function toArray(){
        return get_object_vars($this);
    }

}
