<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class SousFamilleTable {
    //put your code here
    public $tableGateway; // tableGateway de SousFamille
//    public $categoriesTableGateway; // tableGateway de Famille
    
//    public function __construct(TableGateway $tableGateway, $categoriesTableGateway){
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
//        $this->tableGateway = $categoriesTableGateway;
    }
    
    public function obtenirSousFamilles(){
        return($this->tableGateway->select()); 
    }
            
    public function obtenirSousFamillesAvecFiltre($filtre){
        return ($this->tableGateway->select($filtre));
    }
    
    public function obtenirSousFamilleParId($id){
        $sousfamilles = $this->tableGateway->select(['id'=>$id]);
        return ($sousfamilles->current()); // renvoie le 1er objet du resultset
    }
    
//    public function obtenirListeCategoriesPourSousFamilleAvecId($id){
//        return $this->categoriesTableGateway->select(['id_sousfamille'=>$id]);
//    }
    
//    public function obtenirSousFamilleParIdAvecListeInstruments($id){
//        $listeInstruments = [];
//        // on récupère toutes les catégories qui ont cet id de sous-famille
//        $categories = $this->categoriesTableGateway->select(['id_sousfamille'=>$id]);
//        foreach($categories as $objCategorie){
//            $id_cat = $objCategorie->getId();
//            // on récupère la liste d'instruments de chaque catégorie de cette sous-famille
//            $instrumentsCategorie = $this->categoriesTableGateway->obtenirCategorieParIdAvecInstruments($id_cat);
//            foreach($instrumentsCategorie as $objInstrument){
//                // on rajoute chacun de ces instruments à la liste qu'on va attribuer à la sous-famille ensuite
//                $listeInstruments[] = $objInstrument;
//            }
//        }
//        // on récupère l'objet sous-famille concerné
//        $objSousfamille = $this->obtenirSousFamilleParId($id);
//        // on lui injecte la liste d'instruments récupérés
//        $objSousfamille->setListeInstruments($listeInstruments);
//        // et on renvoie l'objet sous-famille qui comporte la liste d'instruments complète
//        return $objSousfamille;
//    }

}
