<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author Violette
 */
class Photo {
    //put your code here
    private $id;
    private $id_instrument;
    private $nom;
    private $size;
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setId_instrument(!empty($data['id_instrument'])?$data['id_instrument']:null);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    function toArray(){
        $photoArray = get_object_vars($this);
        unset($photoArray['size']);
        return $photoArray;
    }
    
    function getId() {
        return $this->id;
    }

    function getId_instrument() {
        return $this->id_instrument;
    }

    function getNom() {
        return $this->nom;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_instrument($id_instrument) {
        $this->id_instrument = $id_instrument;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }
    function getSize() {
        return $this->size;
    }

    function setSize($size) {
        $this->size = $size;
    }



}
