<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class PhotoTable {
    
    public $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function obtenirPhotos(){
        return($this->tableGateway->select()); 
    }
    
    public function obtenirPhotoParId($id){
        $resultat = $this->tableGateway->select(['id'=>$id]);
        return ($resultat->current());
        
    }
    
    public function obtenirPhotosAvecFiltre($filtre){
        return ($this->tableGateway->select($filtre));
    }
    
    public function insererPhoto($objPhoto){
        return $this->tableGateway->insert($objPhoto->toArray());
    }
    
    public function deletePhotosParIdInstrument($idInstrument){
        $resultat=$this->tableGateway->delete(array('id_instrument'=>$idInstrument));
        return ($resultat);
    }

}
