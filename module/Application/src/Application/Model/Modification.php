<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modification
 *
 * @author Violette
 */
class Modification {
    //put your code here
    private $id;
    private $id_loggedUser;
    private $id_utilisateur;
    private $id_famille;
    private $id_sousfamille;
    private $id_categorie;
    private $id_instrument;
    private $datetime_modif;
    private $type_modif;
    private $description;
    
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setId_utilisateur(!empty($data['id_loggedUser'])?$data['id_loggedUser']:null);
        $this->setId_utilisateur(!empty($data['id_utilisateur'])?$data['id_utilisateur']:null);
        $this->setId_famille(!empty($data['id_famille'])?$data['id_famille']:null);
        $this->setId_sousfamille(!empty($data['id_sousfamille'])?$data['id_sousfamille']:null);
        $this->setId_categorie(!empty($data['id_categorie'])?$data['id_categorie']:null);
        $this->setId_instrument(!empty($data['id_instrument'])?$data['id_instrument']:null);
        $this->setDatetime_modif(!empty($data['datetime_modif'])?$data['datetime_modif']:null);
        $this->setType_modif(!empty($data['type_modif'])?$data['type_modif']:null);
        $this->setDescription(!empty($data['description'])?$data['description']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    function getId_loggedUser() {
        return $this->id_loggedUser;
    }

    function setId_loggedUser($id_loggedUser) {
        $this->id_loggedUser = $id_loggedUser;
    }

    function getId() {
        return $this->id;
    }

    function getId_utilisateur() {
        return $this->id_utilisateur;
    }

    function getId_famille() {
        return $this->id_famille;
    }

    function getId_sousfamille() {
        return $this->id_sousfamille;
    }

    function getId_categorie() {
        return $this->id_categorie;
    }

    function getId_instrument() {
        return $this->id_instrument;
    }

    function getDatetime_modif() {
        return $this->datetime_modif;
    }

    function getType_modif() {
        return $this->type_modif;
    }

    function getDescription() {
        return $this->description;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setId_utilisateur($id_utilisateur) {
        $this->id_utilisateur = $id_utilisateur;
    }

    function setId_famille($id_famille) {
        $this->id_famille = $id_famille;
    }

    function setId_sousfamille($id_sousfamille) {
        $this->id_sousfamille = $id_sousfamille;
    }

    function setId_categorie($id_categorie) {
        $this->id_categorie = $id_categorie;
    }

    function setId_instrument($id_instrument) {
        $this->id_instrument = $id_instrument;
    }

    function setDatetime_modif($datetime_modif) {
        $this->datetime_modif = $datetime_modif;
    }

    function setType_modif($type_modif) {
        $this->type_modif = $type_modif;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function toArray(){
        return get_object_vars($this);
    }

    
}
