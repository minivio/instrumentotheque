<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class CategorieTable {
    //put your code here
    public $tableGateway;
    public $instrumentTableGateway;
    
    public function __construct(TableGateway $tableGateway, $instrumentTableGateway){
        $this->tableGateway = $tableGateway;
        $this->instrumentTableGateway = $instrumentTableGateway;
    }
    
    public function obtenirCategories(){
        return($this->tableGateway->select()); 
    }
    
    public function obtenirCategorieParId($id){
        $resultat = $this->tableGateway->select(['id'=>$id]);
        return ($resultat->current());
        
    }
    
    public function obtenirCategoriesAvecFiltre($filtre){
        return ($this->tableGateway->select($filtre));
    }
    
    // renvoie un objet catégorie contenant la liste des instruments de cette catégorie.
    public function obtenirCategorieParIdAvecInstruments($id){
        $objCategorie = $this->obtenirCategorieParId($id);
        $instrumentsFiltres = $this->instrumentTableGateway->select(['id_categorie'=> $objCategorie->getId()]);
        $arrayInstruments = [];
        foreach($instrumentsFiltres as $objInstrument){
            $arrayInstruments[] = $objInstrument;            
        }
        $objCategorie->setListeInstruments($arrayInstruments);
        return($objCategorie);
    }
    
    // renvoie un resultset rempli d'ojets Categorie contenant chacun sa liste d'instruments propres
    public function obtenirToutesLesCategoriesAvecInstruments(){
        $categoriesSansInstruments = $this->obtenirCategories();
        
        $categoriesAvecInstruments = [];
        foreach($categoriesSansInstruments as $objCategorie){
            $idCategorie = $objCategorie->getId();
            $categoriesAvecInstruments[] = $this->obtenirCategorieParIdAvecInstruments($idCategorie);
        }
        return ($categoriesAvecInstruments);
    }

}
