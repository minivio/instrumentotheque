<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class InstrumentTable {
    //put your code here
    public $tableGateway;
    public $categorieTableGateway;
    public $sousfamilleTableGateway;
    public $photoTableGateway;
    public $modificationTableGateway;
    
    public function __construct(TableGateway $tableGateway, $categorieTableGateway, $sousfamilleTableGateway, $photoTableGateway, $modificationTableGateway){
        $this->tableGateway = $tableGateway;
        $this->categorieTableGateway = $categorieTableGateway;
        $this->sousfamilleTableGateway = $sousfamilleTableGateway;
        $this->photoTableGateway = $photoTableGateway;
        $this->modificationTableGateway = $modificationTableGateway;
    }
// ====================================
// ===== INSERT - UPDATE - DELETE =====
// ====================================

    public function insertInstrument($newInstrument){
        $instrumentArray = $newInstrument->toArray();
        $resultat = $this->tableGateway->insert($instrumentArray);
        return ($resultat);
    }

    public function updateInstrument($instrumentModifie){
        $id = $instrumentModifie->getId();  
        return ($this->tableGateway->update($instrumentModifie->toArray(),array('id'=>$id)));
    }
    
    public function deleteInstrument($instrumentObj){
        $id = $instrumentObj->getId();
        // on supprime d'abord toutes les modifications qui auraient été faites sur cet instrument.
        // dans le controller, on ajoute une modification delete qui reprend toutes les infos de l'instrument qu'on va supprimer
        $this->modificationTableGateway->delete(['id_instrument'=>$id]);
        // puis on supprime effectivement l'instrument de la bdd
        $resultat=$this->tableGateway->delete(array('id'=>$id));
        return ($resultat);
    }

// =============================
// ===== SELECT SANS PHOTOS=====
// =============================

    // renvoie 1 objet instrument de l'ID demandé, tel que présent dans la bdd
    public function obtenirInstrumentBaseParId($id){
        return $this->tableGateway->select(['id'=>$id])->current();// renvoie le 1er objet du resultset
    }
    // renvoie une liste d'instruments filtrés, tels que présents dans la bdd
    public function obtenirInstrumentsBaseAvecFiltre($filtre){
        $listeInstruments = $this->tableGateway->select($filtre);        
        return $listeInstruments;
    }
    // renvoie tous les instruments, tels que présents dans la bdd
    public function obtenirInstrumentsBase(){
        return $this->tableGateway->select(); 
    }
    
// =============================
// ===== SELECT AVEC PHOTOS=====
// =============================
// renvoie un array d'objets Photos correspondant à un instrument en particulier
    public function obtenirArrayPhotosParIdInstrument($idInstrument){
        $photos = $this->photoTableGateway->select(['id_instrument'=>$idInstrument]);
        $storeFolder = 'public/img/instruments/';
        $arrayPhotos = [];
        foreach($photos as $objPhoto){
            $nom = $objPhoto->getNom();
            $size = filesize($storeFolder.$nom);
            $objPhoto->setSize($size);
            $arrayPhotos[] = $objPhoto;
        }
        return $arrayPhotos;
    }
    // reçoit une liste d'instruments sans photos, renvoie une liste d'instrument INCLUANT une liste de photos
    public function obtenirListeInstrumentsAvecPhotos($instrumentsSansPhotos){
        $instrumentsAvecPhotos = [];
        foreach ($instrumentsSansPhotos as $objInstrument){
            $arrayPhotos = $this->obtenirArrayPhotosParIdInstrument($objInstrument->getId());
            $objInstrument->setListePhotos($arrayPhotos);
            $instrumentsAvecPhotos[] = $objInstrument;
        }
        return $instrumentsAvecPhotos;
    }

    // renvoie 1 objet instrument de l'ID demandé, INCLUANT la liste des objets photo lui correspondant
    public function obtenirInstrumentParId($id){
        $objInstrument = $this->obtenirInstrumentBaseParId($id);
        $objInstrument->setListePhotos($this->obtenirArrayPhotosParIdInstrument($id));
        return $objInstrument; 
    }
    // renvoie une liste d'instruments filtrés, chacun INCLUANT la liste des objets photo lui correspondant
    public function obtenirInstrumentsAvecFiltre($filtre){
        $instrumentsSansPhotos = $this->obtenirInstrumentsBaseAvecFiltre($filtre);
        return $this->obtenirListeInstrumentsAvecPhotos($instrumentsSansPhotos);
    }
    // renvoie tous les instruments, chacun INCLUANT la liste des objets photo lui correspondant
    public function obtenirInstruments(){
        $instrumentsSansPhotos = $this->obtenirInstrumentsBase();
        return $this->obtenirListeInstrumentsAvecPhotos($instrumentsSansPhotos);
    }
    
// =====================================================
// ===== SELECT INSTRUMENTS ++ (recherche avancée)  =====
// =====================================================
// REMARQUE: il serait intéressant de rajouter le nom de catégorie, famille, sous-famille aux objets instrument
// A suivre, car actuellement pas utilisé dans les vues, donc pas indispensable, mais amélioration souhaitée.

    public function obtenirInstrumentsParIdCategorie($id_categorie, $emprunt_ok){
        $filtre = ($emprunt_ok == 1)? ['id_categorie'=>$id_categorie, 'emprunt_ok'=> $emprunt_ok] : ['id_categorie'=>$id_categorie];
        $listeInstruments = $this->obtenirInstrumentsAvecFiltre($filtre);
        return $listeInstruments;
    }
    
    public function obtenirInstrumentsParIdCategorieAvecNomCategorie($id_categorie, $emprunt_ok){
        // on récupère le nom de la catégorie
        $objCategorie = $this->categorieTableGateway->select(['id'=>$id_categorie])->current();
        $nomCategorie = $objCategorie->getNom();
        // on récupère les instruments de cette catégorie
        $listeInstruments = $this->obtenirInstrumentsParIdCategorie($id_categorie, $emprunt_ok);
        
        return array('listeInstruments'=>$listeInstruments, 
                        'nomCategorie'=>$nomCategorie
                );
    }
    
    // cette fonction est utile dans la fonction suivante (pour obtenir les instruments d'une sous-famille)
    public function obtenirInstrumentsPourArrayIdCategories($listeIdCategories, $emprunt_ok){
        $listeInstruments = [];
        foreach($listeIdCategories as $id_cat){
            $catInstrus = $this->obtenirInstrumentsParIdCategorie($id_cat, $emprunt_ok);
                foreach($catInstrus as $instrument){
                    $listeInstruments[] = $instrument;
                }
        }
        return $listeInstruments;
    }
    
    public function obtenirInstrumentsParIdSousFamilleAvecNomSousFamille($id_sousfamille, $emprunt_ok){
        // on récupère le nom de la sous-famille
        $objSousfamille = $this->sousfamilleTableGateway->select(['id' => $id_sousfamille])->current();
        $nomSousfamille = $objSousfamille->getNom();
        
        // on récupère la liste des catégories appartenant à cette sous-famille
        $listeObjCat = $this->categorieTableGateway->select(['id_sousfamille'=>$id_sousfamille]);
        // on met les ID de ces catégories dans un tableau
        $listeIdCategories = [];
        foreach($listeObjCat as $objCat){
            $idCat = $objCat->getId();
            $listeIdCategories[] = $idCat;
        }
        // on récupère les instruments de chacune des catégories de cette sous-famille
        $listeInstruments = $this->obtenirInstrumentsPourArrayIdCategories($listeIdCategories, $emprunt_ok);
        // on renvoie la liste d'instruments récupérée sous forme de tableau, avec le nom de la sous-famille
        return array('listeInstruments'=>$listeInstruments, 
                     'nomSousfamille'=>$nomSousfamille
                );
    }
    
    
// ===================================
// ===== AUTRES FONCTIONS UTILES =====
// ===================================
    
    public function obtenirNbreInstrumentsParIdCategorie($id_categorie){
        return count($this->obtenirInstrumentsAvecFiltre(['id_categorie'=>$id_categorie]));
    }
    
    public function obtenirNumeroInstrumentParIdCategorie($id_categorie){
        $query = "SELECT SUBSTRING(ref_mc, 13, 3) AS lastNum FROM Instrument WHERE id_categorie = ".$id_categorie." ORDER BY SUBSTRING(ref_mc, 13, 3) DESC LIMIT 1";
        $adapter = $this->tableGateway->getAdapter();
        $resultset = $adapter->query($query, array())->toArray();
        $lastNum = $resultset[0]['lastNum'];
        $nextNum = $lastNum+1;
        
        if($nextNum < 10){
            $nextNum = "00".$nextNum;
        }
        else if($nextNum < 100){
            $nextNum = "0".$nextNum;
        }
        
        return $nextNum;
    }
    
    public function refMcDisponible($ref_mc){
        return (is_object($this->obtenirInstrumentsBaseAvecFiltre(['ref_mc'=>$ref_mc])->current())) ? false : true;
    }
    
    public function getIdLastInserted(){
        //$id = $this->tableGateway->getLastInsertValue(); // étrangement, ceci ne fonctionne pas!
        $adapter = $this->tableGateway->getAdapter();
        $resultset = $adapter->query('SELECT MAX(id) AS lastId FROM Instrument', array())->toArray();
        $id = $resultset[0]['lastId'];
        return $id;
    }
    
    public function getNextId(){
        return $this->getIdLastInserted() + 1;
    }
 
}
