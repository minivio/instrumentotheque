<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway; // permet d'accéder aux fonctions de TableGateway
use Zend\Db\Sql\Select;

// ceci est une classe manager qui accèdera à la bdd.
// cette classe contient un objet TableGateway ainsi que les méthodes du CRUD.
// Ces méthodes utiliseront l'objet TableGateway pour simplifier le code.
// !! l'objet TableGateway n'est pas configuré ici mais dans le service manager (module.config.php)
class UtilisateurTable {
    public $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function obtenirUtilisateurs(){
        return($this->tableGateway->select()); 
    }
    
    public function obtenirUtilisateurParId($id){
        return($this->tableGateway->select(['id' => $id])->current());
    }
    
    public function obtenirUtilisateurParLogin($login){
        return($this->tableGateway->select(['login'=>$login])->current());
    }
    
    public function obtenirUtilisateurAvecFiltre($filtre){
        return($this->tableGateway->select($filtre));
    }
    
    public function obtenirUtilisateursAvecLogin(){
        $select = $this->tableGateway->getSql()->select();
        $select->where->lowerThan('id_role', '3');
        $resultset = $this->tableGateway->selectWith($select);
        return $resultset;
    }

    public function insertUtilisateur($utilisateur){
        return ($this->tableGateway->insert($utilisateur->toArray()));
    }
    
    public function updateUtilisateur($utilisateurModifie){
        $id = $utilisateurModifie->getId();
        return ($this->tableGateway->update($utilisateurModifie->toArray(),array('id'=>$id)));
    }
    
    public function obtenirEmailsNewsletter(){
        $rowset = $this->tableGateway->select(function(Select $select){
            $select->columns(['email']);
            $select->where(['newsletter' => true]);
        });
        return $rowset;
    }
    
    public function getIdLastInsertedUser(){
        //$id = $this->tableGateway->getLastInsertValue(); // étrangement, ceci ne fonctionne pas!
        $adapter = $this->tableGateway->getAdapter();
        $resultset = $adapter->query('SELECT MAX(id) AS lastId FROM Utilisateur', array())->toArray();
        $id = $resultset[0]['lastId'];
        return $id;
    }
    
    // REM: la suppression d'un utilisateur n'est pas souhaitable.
    // Cette fonctionnalité n'est donc volontairement pas implémentée.
}
