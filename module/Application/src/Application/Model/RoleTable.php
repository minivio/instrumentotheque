<?php
namespace Application\Model;
use Zend\Db\TableGateway\TableGateway;

class RoleTable {
    //put your code here
    public $tableGateway;
    
    public function __construct(TableGateway $tableGateway){
        $this->tableGateway = $tableGateway;
    }
    
    public function obtenirRoles(){
        return($this->tableGateway->select()); 
    }
    
    public function obtenirRoleParId($id){
        return($this->tableGateway->select(['id' => $id])->current()); 
    }
    
    public function obtenirIntituleRoleParId($id){
        $objRole = $this->obtenirRoleParId($id);
        return $objRole->getIntitule();
    }
}
