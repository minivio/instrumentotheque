<?php
namespace Application\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorie
 *
 * @author Violette
 */
class Categorie {
    //put your code here
    private $id;
    private $legende;
    private $id_sousfamille;
    private $id_famille;
    private $nom;
    private $infos_comp1;
    private $infos_comp2;
    private $video;
    private $listeInstruments = [];
    
    public function exchangeArray($data){
        $this->setId(!empty($data['id'])?$data['id']:null);
        $this->setLegende(!empty($data['legende'])?$data['legende']:null);
        $this->setId_sousfamille(!empty($data['id_sousfamille'])?$data['id_sousfamille']:null);
        $this->setNom(!empty($data['nom'])?$data['nom']:null);
        $this->setInfos_comp1(!empty($data['infos_comp1'])?$data['infos_comp1']:null);
        $this->setInfos_comp2(!empty($data['infos_comp2'])?$data['infos_comp2']:null);
        $this->setVideo(!empty($data['video'])?$data['video']:null);
    }
    
    public function __construct($donnees=[]){
        $this->hydrate($donnees);
    }
    
    public function hydrate(array $donnees){
        foreach($donnees as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    } 
    
    function getId() {
        return $this->id;
    }
    function getLegende() {
        return $this->legende;
    }
    function getId_sousfamille() {
        return $this->id_sousfamille;
    }
    
    function getId_famille() {
        return $this->id_famille;
    }
        function getNom() {
        return $this->nom;
    }

    function getInfos_comp1() {
        return $this->infos_comp1;
    }

    function getInfos_comp2() {
        return $this->infos_comp2;
    }

    function getVideo() {
        return $this->video;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setLegende($legende) {
        $this->legende = $legende;
    }

    function setId_sousfamille($id_sousfamille) {
        $this->id_sousfamille = $id_sousfamille;
    }

    function setId_famille($id_famille) {
        $this->id_famille = $id_famille;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setInfos_comp1($infos_comp1) {
        $this->infos_comp1 = $infos_comp1;
    }

    function setInfos_comp2($infos_comp2) {
        $this->infos_comp2 = $infos_comp2;
    }

    function setVideo($video) {
        $this->video = $video;
    }

    function getListeInstruments() {
        return $this->listeInstruments;
    }

    function setListeInstruments($listeInstruments) {
        $this->listeInstruments = $listeInstruments;
    }

    
    function toArray(){
        return get_object_vars($this);
    }


}
