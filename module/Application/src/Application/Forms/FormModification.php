<?php
// indiquer le namespace de la classe
namespace Application\Forms;
// utilisation de classes de Zend2: Form et Element
use Zend\Form\Form;
use Zend\Form\Element;

class FormModification extends Form {

   
    // appel au constructeur de la superclasse Form pour construir l'objet de base
    public function __construct($name = null, $sm = null) {
        parent::__construct($name);        
        
        if($sm!=null){
            $id_loggedUser = new Element\Select('id_loggedUser');
            $id_loggedUser->setLabel('Modifications effectuées par: ');
            $id_loggedUser->setAttributes(array('id'=>'id_loggedUser'));
            
            $id_utilisateur = new Element\Select('id_utilisateur');
            $id_utilisateur->setLabel('Utilisateur modifié');
            $id_utilisateur->setAttributes(array('id'=>'id_utilisateur'));
            
            $utilisateurs = $sm->get('UtilisateurTableCRUD')->obtenirUtilisateurs();
            $arrayOptionsUsersWithLogin = [];
//            $arrayOptionsAllUsers = [];
            foreach($utilisateurs as $objUser){
                $id = $objUser->getId();
                $intitule = $objUser->getLogin()." - ".$objUser->getNom();
//                $arrayOptionsAllUsers[$id] = $intitule;
                if($objUser->getId_role() < 3){$arrayOptionsUsersWithLogin[$id] = $intitule;}
            }
            
            $id_loggedUser->setValueOptions($arrayOptionsUsersWithLogin);
            $id_loggedUser->setEmptyOption("Sélectionnez un utilisateur...");
            $this->add($id_loggedUser);
            
//            $id_utilisateur->setValueOptions($arrayOptionsAllUsers);
//            $id_utilisateur->setEmptyOption("Sélectionnez un utilisateur...");
//            $this->add($id_utilisateur);
            
            
//            $id_instrument = new Element/Select('id_instrument');
//            $id_instrument->setLabel('Instrument modifié');
//            $id_instrument->setAttributes(array('id'=>'id_instrument'));
//            
//            $instruments = $sm->get('InstrumentTableCRUD')->obtenirInstruments();
//            $arrayOptionsInstruments = [];
//            foreach($instruments as $objInstrument){
//                $id = $objInstrument->getId();
//                $intitule = $objInstrument->getRef_mc()." | ".$objInstrument->getNom();
//                $arrayOptionsInstruments[$id] = $intitule;
//            }
//            
//            $id_instrument->setValueOptions($arrayOptionsInstruments);
//            $id_instrument->setEmptyOption("Sélectionnez un instrument...");
//            $this->add($id_instrument);
            
            
            
        }
        
        $dateDebut = new Element\Date('dateDebut');
        $dateDebut
                ->setLabel('Entre le')
                ->setOptions(['format' => 'd-m-Y']);
        $this->add($dateDebut);
        
        $dateFin = new Element\Date('dateFin');
        $dateFin
                ->setLabel('et le')
                ->setOptions(['format' => 'd-m-Y']);
        $this->add($dateFin);
        
        
        $sendButton= new Element ('boutonEnvoyer');
        $sendButton->setAttributes(array('type'=>'submit', 'value'=>'Envoyer'));
        $this->add($sendButton);
    }  
}

