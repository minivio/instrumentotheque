<?php
// indiquer le namespace de la classe
namespace Application\Forms;
// utilisation de classes de Zend2: Form et Element
use Zend\Form\Form;
use Zend\Form\Element;

class FormUtilisateur extends Form {

   
    // appel au constructeur de la superclasse Form pour construir l'objet de base
    public function __construct($name = null, $sm = null) {
        parent::__construct($name);        
        
        if($sm!=null){
            $id_role = new Element\Select('id_role');
            $id_role->setLabel('Rôle');
            $id_role->setAttributes(array('id'=>'id_role'));
            $roles = $sm->get('RoleTableCRUD')->obtenirRoles();
            $arrayOptions = [];
            foreach($roles as $objRole){
                $id = $objRole->getId();
                $intitule = $objRole->getIntitule();
                $arrayOptions[$id] = $intitule;
            }
            $id_role->setValueOptions($arrayOptions);
            $id_role->setEmptyOption("Sélectionnez un rôle...");
            $this->add($id_role);
        }

        $nom = new Element('nom');
        $nom->setOptions(array ('label'=>'Nom')); 
        $nom->setAttributes(array('type' => 'text', 'id'=>'nom'));
        $this->add($nom);
                
        $email= new Element\Email('email');
        $email->setOptions(array ('label'=>'Email')); 
        $email->setAttributes(array('id'=>'email'));
        $this->add($email);
        
        $email_check= new Element\Email('email_check');
        $email_check->setOptions(array ('label'=>'Email')); 
        $email_check->setAttributes(array('id'=>'email_check'));
        $this->add($email_check);
        
        $tel= new Element('tel');
        $tel->setOptions(array ('label'=>'Tel')); 
        $tel->setAttributes(array('type' => 'tel', 'id'=>'tel'));
        $this->add($tel);
        
        $newsletter= new Element\Checkbox('newsletter');
        $newsletter->setOptions(array ('label'=>'Newsletter')); 
        $newsletter->setAttributes(array('checked'=>'checked', 'id'=>'newsletter'));
        $this->add($newsletter);
        
        $login= new Element('login');
        $login->setOptions(array ('label'=>'Login')); 
        $login->setAttributes(array('type' => 'text', 'id'=>'login'));
        $this->add($login);
        
        $mdp= new Element\Password('mdp'); 
        $mdp->setOptions(array ('label'=>'Mot de passe')); 
        $mdp->setAttributes(['id'=>'mdp']);
        $this->add($mdp);
        
        $mdp_check= new Element\Password('mdp_check'); 
        $mdp_check->setOptions(array ('label'=>'Vérification mot de passe')); 
        $mdp_check->setAttributes(['id'=>'mdp_check']);
        $this->add($mdp_check);
        
        $sendButton= new Element\Button ('boutonEnvoyer');
        $sendButton->setAttributes(array('type'=>'button', 'value'=>'Envoyer'));
        $this->add($sendButton);
    }

    
    }

