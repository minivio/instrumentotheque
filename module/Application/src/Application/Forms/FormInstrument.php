<?php
// indiquer le namespace de la classe
namespace Application\Forms;
// utilisation de classes de Zend2: Form et Element
use Zend\Form\Form;
use Zend\Form\Element;

class FormInstrument extends Form {
    
//    private $sm;
    
// appel au constructeur de la superclasse Form pour construir l'objet de base
public function __construct($sm, $name= null) {
    parent::__construct($name);

    $select = new Element\Select('id_categorie');
    $select->setAttributes(array('id'=>'id_categorie'));
    $arrayOptions1 = [];
    $familles = $sm->get("FamilleTableCRUD")->obtenirFamilles();
    foreach($familles as $famille){
        $idFamille = $famille->getId();
        $nomFamille = $famille->getNom();
        $legendeFamille = $famille->getLegende();
        $arrayOptions1[$nomFamille] = ['label'=> strtoupper($nomFamille." ".$legendeFamille)];

        $sousfamilles = $sm->get("SousFamilleTableCRUD")->obtenirSousFamillesAvecFiltre(['id_famille'=>$idFamille]);

        foreach($sousfamilles as $sousfamille){

            $idSousfamille = $sousfamille->getId();
            $nomSousfamille = $sousfamille->getNom();
            $legendeSousFamille = $sousfamille->getLegende();
            $arrayOptions1[$nomFamille]['options'][$idSousfamille] =  ['label'=>$nomSousfamille." ".$legendeSousFamille, 'options' => []];

            $categories = $sm->get("CategorieTableCRUD")->obtenirCategoriesAvecFiltre(['id_sousfamille'=>$idSousfamille]);
            foreach($categories as $categorie){
                $idCategorie = $categorie->getId();
                $nomCategorie = $categorie->getNom();
                $legendeCategorie = $categorie->getLegende();
//                $arrayOptions1[$nomFamille]['options'][$idSousfamille]['options'][$idCategorie] = $legendeCategorie." ".$nomCategorie;
                $arrayOptions1[$nomFamille]['options'][$idSousfamille]['options'][$idCategorie]['attributes'] = ['data-ref'=>$legendeFamille.$legendeSousFamille.$legendeCategorie];
                $arrayOptions1[$nomFamille]['options'][$idSousfamille]['options'][$idCategorie]['value'] = $idCategorie;
                $arrayOptions1[$nomFamille]['options'][$idSousfamille]['options'][$idCategorie]['label'] = $legendeCategorie." ".$nomCategorie;
            }
        }
    }
    $select->setValueOptions($arrayOptions1);
    $select->setEmptyOption("...");
    $this->add($select);

// Pour afficher la liste des sous-familles classées par famille
    $selectSsFam = new Element\Select('id_sousfamille');
    $selectSsFam->setAttributes(array('id'=>'id_sousfamille'));
    $arrayOptions2 = [];
    $listeFamilles = $sm->get("FamilleTableCRUD")->obtenirFamilles();
    foreach($listeFamilles as $famille){
        $idFamille = $famille->getId();
        $nomFamille = $famille->getNom();
        $arrayOptions2[$nomFamille] = ['label'=> strtoupper($nomFamille)];

        $sousfamilles = $sm->get("SousFamilleTableCRUD")->obtenirSousFamillesAvecFiltre(['id_famille'=> $idFamille]);
        foreach($sousfamilles as $sousfamille){
            $idSousfamille = $sousfamille->getId();
            $nomSousfamille = $sousfamille->getNom();
            $arrayOptions2[$nomFamille]['options'][$idSousfamille] = $nomSousfamille;
        }
    }
    $selectSsFam->setValueOptions($arrayOptions2);
    $selectSsFam->setEmptyOption("-- Choisissez une famille --");
    $this->add($selectSsFam);

    $inputRefMc= new Element('ref_mc');
    $inputRefMc->setAttributes(array('type' => 'text','id'=>'ref_mc', 'placeholder'=>'Ex: MC11.V-BOI.A014'));
    $this->add($inputRefMc);

    $inputRefMf= new Element('ref_mf');
    $inputRefMf->setAttributes(array('type' => 'text','id' => 'ref_mf', 'placeholder'=>'*optionnel'));
    $this->add($inputRefMf);

    $inputNom= new Element('nom');
    $inputNom->setAttributes(array('type' => 'text', 'id'=>'nom', 'placeholder' => 'Ex: flûte longue'));
    $this->add($inputNom);

    $inputDetail= new Element\Textarea('detail_description');
    $inputDetail->setAttributes(array('id'=>'detail_description', 'placeholder'=>'Ex: à motifs géométriques et abstraits en incise'));
    $this->add($inputDetail);

    $inputAccessoires= new Element\Textarea('accessoires');
    $inputAccessoires->setAttributes(array('id'=>'accessoires', 'placeholder'=>'Ex: étui en cuir'));
    $this->add($inputAccessoires);

    $emprunt= new Element\Radio('emprunt_ok');
    $emprunt->setValueOptions([
        '0'=>'consultation sur place uniquement',
        '1'=>'emprunt possible'
    ]); 
    $emprunt->setAttributes(array('id'=>'emprunt_ok'));
    $this->add($emprunt);

    $inputInfos1= new Element\Textarea('infos_comp1');
    $inputInfos1->setAttributes(array('id'=>'infos_comp1', 'placeholder'=>'Ex: Le naï roumain est une flûte incurvée à une rangée...'));
    $this->add($inputInfos1);

    $inputInfos2= new Element\Textarea('infos_comp2');
    $inputInfos2->setAttributes(array('id'=>'infos_comp2', 'placeholder'=>'Ex: La base des tubes vient se loger dans une embase c...'));
    $this->add($inputInfos2);

    $inputVideoLink= new Element('videoLink');
    $inputVideoLink->setAttributes(array('id'=>'videoLink','type' => 'text', 'placeholder'=>'Ex: https://www.youtube.com/embed/lbWlYz9Zv5c'));
    $this->add($inputVideoLink);

    $inputAudioLink= new Element('audioLink');
    $inputAudioLink->setAttributes(array('id'=>'audioLink','type' => 'text', 'placeholder'=>'Lien vers un fichier audio online'));
    $this->add($inputAudioLink);

    $filePic = new Element\File('photoFile');
    $filePic->setAttributes(['type'=>'file', 'id'=>'photo']);
    $this->add($filePic);

    $fileVideo = new Element\File('videoFile');
    $fileVideo->setAttributes(['type'=>'file', 'id'=>'videoFile']);
    $this->add($fileVideo);

    $fileAudio = new Element\File('audioFile');
    $fileAudio->setAttributes(['type'=>'file', 'id'=>'audioFile']);
    $this->add($fileAudio);

    $sendButton= new Element ('boutonEnvoyer');
    $sendButton->setAttributes(array('type'=>'submit', 'value'=>'Envoyer'));
    $this->add($sendButton);
    }
}

