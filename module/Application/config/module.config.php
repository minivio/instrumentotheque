<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'route'    => '/[:controller[/:action[/:id][/:id2]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9a-zA-Z]*',
                                'id2' => '[0-9a-zA-Z]*'
                            ),
                            'defaults' => array(
                                /*'NAMESPACE'=>'Application\Controller',
                                'controller'=>'Test',
                                'action'=>'index',
                                'id'=>*/
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories'=>array(
            //'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
//             service pour créer l'objet TableGateway dont le Modèle Utilisateur a besoin.
            'UtilisateurTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');// obtention de l'adapter
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet(); // création d'un objet resultSet vide
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Utilisateur()); // on indique que le ResultSet contiendra des objets de la classe de base du modèle Utilisateur 
              //on configure l'objet TableGateway en y injectant notamment l'adapter qu'on vient de créer, dont la configuration se trouve dans local.php
              // renvoie un nouveau TableGateway
              return new \Zend\Db\TableGateway\TableGateway('Utilisateur', $adapter, null, $resultSetPrototype);
            },        
//             création d'un service qui appelle un autre service
            'UtilisateurTableCRUD'=>function($sm){
                  // on obtient le service qui crée le TableGateway
                $tableGateway = $sm->get('UtilisateurTableGateway');
                // on crée et on renvoie un objet de la classe du modèle UtilisateurTable dans lequel on a injecté l'objet TableGateway.
                $utilisateurManager = new \Application\Model\UtilisateurTable($tableGateway);
                return $utilisateurManager;
            },
            'FamilleTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Famille()); 
              return new \Zend\Db\TableGateway\TableGateway('Famille', $adapter, null, $resultSetPrototype);
           },        
            'FamilleTableCRUD'=>function($sm){
                $tableGateway = $sm->get('FamilleTableGateway');
                $familleManager = new \Application\Model\FamilleTable($tableGateway);
                return $familleManager;
            },
            'SousFamilleTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\SousFamille());
              return new \Zend\Db\TableGateway\TableGateway('SousFamille', $adapter, null, $resultSetPrototype);
           },        
            'SousFamilleTableCRUD'=>function($sm){
                $tableGateway = $sm->get('SousFamilleTableGateway');
//                $categorieTableGateway = $sm->get('CategorieTableGateway');
//                $sousfamilleManager = new \Application\Model\SousFamilleTable($tableGateway, $categorieTableGateway);
                $sousfamilleManager = new \Application\Model\SousFamilleTable($tableGateway);
                return $sousfamilleManager;
            },
            'CategorieTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Categorie());
              return new \Zend\Db\TableGateway\TableGateway('Categorie', $adapter, null, $resultSetPrototype);
           },                           
            'CategorieTableCRUD'=>function($sm){
                $tableGateway = $sm->get('CategorieTableGateway');
                $instrumentTableGateway=$sm->get('InstrumentTableGateway'); 
                $categorieManager = new \Application\Model\CategorieTable($tableGateway, $instrumentTableGateway);
                return $categorieManager;
            },
            'PhotoTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Photo());
              return new \Zend\Db\TableGateway\TableGateway('Photo', $adapter, null, $resultSetPrototype);
           },        
            'PhotoTableCRUD'=>function($sm){
                $tableGateway = $sm->get('PhotoTableGateway');
                $photoManager = new \Application\Model\PhotoTable($tableGateway);
                return $photoManager;
            },
            'InstrumentTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Instrument());
              return new \Zend\Db\TableGateway\TableGateway('Instrument', $adapter, null, $resultSetPrototype);
           },        
            'InstrumentTableCRUD'=>function($sm){
                $tableGateway = $sm->get('InstrumentTableGateway');
                $categorieTableGateway = $sm->get('CategorieTableGateway');
                $sousfamilleTableGateway = $sm->get('SousFamilleTableGateway');
                $photoTableGateway = $sm->get('PhotoTableGateway');
                $modificationTableGateway = $sm->get("ModificationTableGateway");
                $instrumentManager = new \Application\Model\InstrumentTable($tableGateway, $categorieTableGateway, $sousfamilleTableGateway, $photoTableGateway, $modificationTableGateway);
                return $instrumentManager;
            },
            'ModificationTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Modification());
              return new \Zend\Db\TableGateway\TableGateway('Modification', $adapter, null, $resultSetPrototype);
           },        
            'ModificationTableCRUD'=>function($sm){
                $tableGateway = $sm->get('ModificationTableGateway');
                $modificationManager = new \Application\Model\ModificationTable($tableGateway);
                return $modificationManager;
            },
            'RoleTableGateway'=>function($sm){
              $adapter=$sm->get('Zend\Db\Adapter\Adapter');
              $resultSetPrototype = new Zend\Db\ResultSet\ResultSet();
              $resultSetPrototype->setArrayObjectPrototype(new \Application\Model\Role());
              return new \Zend\Db\TableGateway\TableGateway('Role', $adapter, null, $resultSetPrototype);
           },        
            'RoleTableCRUD'=>function($sm){
                $tableGateway = $sm->get('RoleTableGateway');
                $roleManager = new \Application\Model\RoleTable($tableGateway);
                return $roleManager;
            },
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Utilisateur' => 'Application\Controller\UtilisateurController',
            'Application\Controller\Catalogue' => 'Application\Controller\CatalogueController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
