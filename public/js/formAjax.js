$(document).ready(function(){ 
// =======================
// ===== INSTRUMENTS =====
// =======================
// <editor-fold defaultstate="collapsed" desc="FORMULAIRE DE RECHERCHE AVANCEE >>>> appels controllers">
// ===========================================
// ===== FORM. RECH. AVANCEE INSTRUMENTS =====
// ===========================================

    // >>>>> permet de peupler la liste catégories en fonction de la sous-famille sélectionnée <<<<<
    $('#id_sousfamille').change(function(){  
        $.ajax({
            url: '/application/catalogue/rechercheCategoriesAjax',
            method: 'POST',
            dataType: 'json',
            data:{
                id_sousfamille : $('#id_sousfamille').val()
            },
            success: function (data){
                var catSelect = $('#categories_ajax');
                catSelect.empty();
                var newCatOptions = [$('<option value="">-- Choisissez une catégorie --</option>')];
                for(var i = 0, len = data['categories'].length; i < len; i++){
                    newCatOptions.push($('<option value="'+data['categories'][i].id+'">'+data['categories'][i].nom+'</option>'));
                }
                catSelect.append(newCatOptions);   
                catSelect.removeAttr('disabled');
            },
            error: function(data){
                alert("error");
            }
        }); 
    });
    
 //>>>>> Récupération des résultats d'une recherche avancée dans le catalogue
    $("#btnRechercheAvancee").click(function(){       
        $.ajax({
            url: '/application/catalogue/resultatRechercheInstrumentAjax',
            method: 'POST',
            dataType: 'text',
            data:{
                // données récupérées du formulaire de recherche avancée
                id_sousfamille : $('#id_sousfamille').val(),
                id_categorie : $('#categories_ajax').val(),
                ref_mc: $('#ref_mc').val(),
                emprunt_ok: $('input[name="emprunt_ok"]:checked').val()
            },
            success: function (data){
                // on cible le div dans lequel on va insérer le résultat de la requête.
                var divResultatRecherche = $("#resultatRechercheAvancee");
                // on le vide préalablement de son contenu
                divResultatRecherche.empty();
                divResultatRecherche.html(data);
                $('body, html').animate({scrollTop : $("#resultatRechercheAvancee").offset().top - 70 }, 'slow');
            },
            error: function(data){
//                alert("error");
                console.log(data);
            }
        });
    });
// </editor-fold>  
 
// <editor-fold defaultstate="collapsed" desc="FORMULAIRE D'INSERTION D'UN NOUVEL INSTRUMENT >>>> vérifications et appels controllers">
// =============================================
// ===== FORMULAIRE D'INSERTION INSTRUMENT =====
// =============================================
function requiredInsertFieldsValid(){
    return ($('#id_categorieInsert').val() == "" || $('#nomInsert').val() == "" || $('#ref_mcInsert').val() == "")? false : true; 
}

var refStart = "MC**";
var refMiddle = ".*-***.*";
var refEnd = "***";

function afficherRefMc(){
    var fullRef = refStart+refMiddle+refEnd;
    $('#ref_mcInsert').val(fullRef);
}
// quand une catégorie est sélectionnée, 
// on va rechercher le nombre d'instruments déjà dans cette catégorie
// afin de déterminer les 3 derniers chiffres de la référénce
// on affiche la référence partielle qui a été générée
$('#id_categorieInsert').change(function(){
    refMiddle = $('#id_categorieInsert option:selected').data("ref");        
    $.ajax({
        url: '/application/catalogue/determinerNumExemplaire',
        method: 'POST',
        dataType: 'text',
        data:{
            id_categorie : $('#id_categorieInsert').val(),
        },
        success: function(data){
            refEnd = data;
            afficherRefMc();
        },
        error: function(data){
            alert("erreur Ajax insertion");
        }
    });

});
// quand l'année d'acquisition est complétée,
// on complète la référence et on l'affiche
$('#annee').change(function(){
    var annee = $('#annee').val();
    refStart = "MC"+annee.substring(2,4);        
    afficherRefMc();
});

// il est possible de modifier manuellement une référence en cliquant sur le bouton "modifier"
// le champs ref_mc est à nouveau éditable
$('#enableRef').click(function(){
   $('#ref_mcInsert').removeAttr('disabled'); 
});

// si le champs "ref_mc" est modifié,
// on vérifie qu'une référence identique ne se trouve pas déjà dans la base de données
$('#ref_mcInsert').change(function(){
    $.ajax({
        url: '/application/catalogue/verifierRefMc',
        method: 'POST',
        dataType: 'text',
        data:{
            ref_mc : $('#ref_mcInsert').val(),
        },
        success: function(data){
            if(data == false){
                $('#ref_mcInsert').parents('.form-group').addClass('has-error');
                $('#ref_mcInsert+span').hide();
                $('#refError').show();

            }
            else{
                $('#ref_mcInsert').parents('.form-group').removeClass('has-error');
                $('#ref_mcInsert+span').show();
                $('#refError').hide();
            }
        },
        error: function(data){
            alert("erreur Ajax insertion");
        }
    });
});

// lorsqu'on valide le formulaire,
// on vérifie d'abord que tous les champs requis sont complétés,
// puis on envoie les données au controller pour insertion dans la bdd
$("#insertBtn").click(function(){
    if(requiredInsertFieldsValid()){
        $.ajax({
            url: '/application/catalogue/insertInstrument',
            method: 'POST',
            dataType: 'text',
            data:{
                // données récupérées du formulaire d'insertion
                id_categorie : $('#id_categorieInsert').val(),
                nom : $('#nomInsert').val(),
                detail_description : $('#detail_descriptionInsert').val(),
                accessoires : $('#accessoiresInsert').val(),
                ref_mc : $('#ref_mcInsert').val(),
                ref_mf : $('#ref_mfInsert').val(),
                emprunt_ok: $('input[name="emprunt_okInsert"]:checked').val(),
                infos_comp1 : $('#infos1Insert').summernote('code'),
                infos_comp2 : $('#infos2Insert').summernote('code'),
                video : $('#videoInsert').val()
            },
            success: function(data){
                if(data){
                    $('#insertErrorMsg').hide();
                    $('#insertSuccessMsg').show();
                    setTimeout(function(){
                        window.location.reload();
                    }, 4500);
                }
                else{
                    $('#insertErrorMsg').show();
                }
                
            },
            error: function(data){
                alert("erreur Ajax insertion");
                console.log(data);
            }
        });
    }
    else{
        $('#insertErrorMsg').show();
    }
});


    
    
//</editor-fold>
  
// <editor-fold defaultstate="collapsed" desc="BOUTON VOIR FICHE COMPLETE >>>> appels controllers">

$(document).delegate('.getfullinfo', 'click', function(){
    var idInstrument = $(this).parent().next().val();
        $.ajax({
            url:'/application/catalogue/afficherInstrumentParId',
            method:'POST',
            datatype:'text',
            data:{
                id: idInstrument
            },
            success: function(data){
                // on cible le div dans lequel on va insérer le résultat de la requête.
                var divResultatRecherche = $("#ficheInstruComplete");
                // on le vide préalablement de son contenu
                divResultatRecherche.empty();
                divResultatRecherche.html(data);
            },
            error: function(data){
                alert('ERREUR');
            }
        });
});

//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="BOUTON UPDATE INSTRUMENT >>>> appels controllers">
$(document).delegate('.updateinfoInstru', 'click', function(){
    $.ajax({
        url:'/application/catalogue/afficherFormUpdateInstrumentAvecDropzone',
        method:'POST',
        datatype:'text',
        data:{
            id: $(this).parent().next().val()
        },
        success: function(data){
            // on cible le div dans lequel on va insérer le html obtenu.
            var cible = $("#updateInstruModal");
            // on le vide préalablement de son contenu
            cible.empty();
            cible.html(data);
        },
        error: function(data){
            alert('ERREUR UPDATE FORM');
        }
    });
});

$(document).delegate('#submitUpdate', 'click', function(e){
    e.preventDefault();
    $.ajax({
        url: '/application/catalogue/updateInstrument',
        method: 'POST',
        dataType: 'text',
        data:{
            id: $('#idInstrumentUpdate').val(),
            id_categorie : $('#id_categorieUpdate').val(),
            nom : $('#nomUpdate').val(),
            detail_description : $('#detail_descriptionUpdate').val(),
            accessoires : $('#accessoiresUpdate').val(),
            ref_mc : $('#ref_mcUpdate').val(),
            ref_mf : $('#ref_mfUpdate').val(),
            emprunt_ok: $('input[name="emprunt_okUpdate"]:checked').val(),
            infos_comp1 : $('#infos1Update').summernote('code'),
            infos_comp2 : $('#infos2Update').summernote('code'),
            video : $('#videoUpdate').val()
        },
        success: function (data){
            $('#ficheInstruComplete').modal('hide');
            $('#updateSuccessMsg').show();
            setTimeout(function(){
//                $('#updateInstruModal').modal('hide');
                window.location.reload();
            }, 3000);
        },
        error: function(data){
            alert("error");
        }
    }); 
});
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="BOUTON DELETE INSTRUMENT >>>> appels controllers">
$(document).delegate('.deleteInstru', 'click', function(){
    var id= $(this).parent().next().val();
    $('#confimDeleteInstru').click(function(){
        $.ajax({
            url:'/application/catalogue/deleteInstrument',
            method:'POST',
            datatype:'text',
            data:{
                id: id
            },
            success: function(data){
                $('#ficheInstruComplete').modal('hide');
                $('#deleteConfirmAlert').show();
                $('#deleteInstruModal .modal-footer button').hide();
                setTimeout(function(){
                    $('#deleteInstruModal').modal('hide');
                    window.location.reload();
                }, 2000);
            },
            error: function(data){
                alert('ERREUR'+ data);
                console.log(data);
            }
        });
    }); 
});
//</editor-fold>

// ==========================
// ====== UTILISATEURS ======
// ==========================

// <editor-fold defaultstate="collapsed" desc="SE CONNECTER / SE DECONNECTER >>>> vérifications et appel controller">
// ===========================================
// ====== SE CONNECTER / SE DECONNECTER ======
// ===========================================

// >>>>> formulaire de connexion <<<<<
// permet de vérifier si le login et le mot de passe sont corrects.
// si correct, 
//      > mise à jour des données de session
//      > fermeture de la fenêtre modale (formulaire de connexion)
//      > affichage du menu utilisateur dans la barre de navigation (avec le login de l'utilisateur)
// si incorrect, 
//      > insertion d'un message dans le formulaire de connexion (fenêtre modale)

    $("#loginForm #btn").on("click", function(){
        
        $.ajax({
            url:'/application/utilisateur/loginFormResult',
            method:'POST',
            datatype:'text',
            data:{
                login : $('#login').val(),
                mdp: $('#mdp').val()
            },
            success: function(data){
                
                // data = chaîne de caractères vide si login ou mdp ne correspondent pas
                // data = login de l'utilisateur qui vient de s'enregistrer
                
                // ds tous les cas, on s'assure que le paragraphe de message d'erreur soit bien vidé
                var msgP = $("#loginMsg");
                msgP.empty();
                // si pas bon, on insère un message ds le paragraphe de message d'erreur de la fenêtre modale
                if(data == ""){
                    msgP.append("Login et/ou mot de passe incorrect(s)");   
                }
                // si c'est ok, alors...
                else{
                    $("#loginLink").hide(); // on cache le lien se connecter
                    $("#loginForm").modal('hide'); // on referme la fenêtre modale
                    $("#userProfileLink").toggleClass('hidden'); // on affiche le dropdown user 
                    $("#userProfileLink span").first().after(document.createTextNode(" "+ data.toUpperCase() +" ")); // on insère le nom d'utilisateur
                    window.location.reload();
                }
            },
            error: function(data){
                alert('ERREUR');
            }
        });
    });
    
    $('#seeProfile').click(function(){
        $.ajax({
            url:'/application/utilisateur/profilPersonnel',
            method:'POST',
            datatype:'text',
            data:{
            },
            success: function(data){
                var cible = $('#loggedUserProfile .modal-body');
                cible.empty();
                // si pas bon, on insère un message ds le paragraphe de message d'erreur de la fenêtre modale
                if(data != ""){
                    cible.html(data);   
                }
                // si c'est ok, alors...
                else{
                    cible.text("ZUT"); 
                }
            },
            error: function(data){
                alert('ERREUR');
            }
        });
    });
    
// >>>>> formulaire de déconnexion <<<<<
// destruction des données de session utilisateur (ds le controller)
// affichage du bouton login à la place du menu utilisateur
// effacement du login dans le bouton du menu utilisateur caché
    $("#logoutLink").click(function(e){
        e.preventDefault();
        $.ajax({
            url:'/application/utilisateur/logout',
            method:'POST',
            datatype:'text',
            data:{
            },
            success: function(data){
                window.location.reload();
                $("#loginLink").show();
                $("#userProfileLink").toggleClass('hidden'); 
                $($("#userProfileLink").contents()[1]).remove(); // on enlève le login d'utilisateur               
            },
            error: function(data){
                alert('ERREUR');
            }
        });
    }); 
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="VERIFICATIONS FORMULAIRES INSERT & UPDATE UTILISATEUR >>>> vérifications et appels controller">
// call ajax permettant de vérifier si un login est disponible ou non (via controller)
    function checkLogin(){
        $.ajax({
            url:'/application/utilisateur/loginDispo',
            method:'POST',
            datatype:'text',
            data:{
                login : $('#new_login').val()
            },
            success: function(data){
                var login = $('<p>'+login+'</p>');
                if(data){
                    $('#new_login').parents('.form-group').removeClass('has-error');  
                    $('#new_login+.help-block').hide();
                }
                else{
                    $('#new_login').parents('.form-group').addClass('has-error');
                    $('#new_login+.help-block').show();
                }
            },
            error: function(data){
                alert('ERREUR');
            }
        });
    }
    
    function checkMdp(){
        var first = $('#new_mdp').val();
        var second = $('#mdp_check').val();
        if(first === second){
            $('#new_mdp').parents('.form-group').removeClass('has-error');
            $('#mdp_check').parents('.form-group').removeClass('has-error');
            $('#mdp_check+.help-block').hide();
        }
        else{
            $('#new_mdp').parents('.form-group').addClass('has-error');
            $('#mdp_check').parents('.form-group').addClass('has-error');
            $('#mdp_check+.help-block').show();
        }
    }
    
    function checkEmailInsert(){
        var first = $('#new_email').val();
        var second = $('#email_check').val();
        if(first === second){
            $('#new_email').parents('.form-group').removeClass('has-error');
            $('#email_check').parents('.form-group').removeClass('has-error');
            $('#email_check+.help-block').hide();
        }
        else{
            $('#new_email').parents('.form-group').addClass('has-error');
            $('#email_check').parents('.form-group').addClass('has-error');
            $('#email_check+.help-block').show();
        }
    }
    
    
    function checkIdRole(){
        var selected = false;
        if($('#new_id_role').val() === ""){
            $('#new_id_role').parents('.form-group').addClass('has-error');
            $('#new_id_role+.help-block').show();
        }
        else{
            selected = true;
        }
        return selected;
    }
    
    function checkNewsletterAndEmail(){
        if($('#new_newsletter').is(':checked') && $('#new_email').val() === ""){
            $('#new_email').parents('.form-group').addClass('has-error');
            $('#email_check').parents('.form-group').addClass('has-error');
            return false;
        }
        else{
            $('#new_email').parents('.form-group').removeClass('has-error');
            $('#email_check').parents('.form-group').removeClass('has-error');
            return true;
        }
    }
    
    function checkRequirementsByRole(){
        var isValid = true;
        
        switch($('#new_id_role').val()){
            case "1":
            case "2":
                if($('#new_login').val() === ""){
                    $('#new_login').parents('.form-group').addClass('has-error');
                    isValid = false;
                }
                if($('#new_mdp').val() === "" || $('#mdp_check').val() === ""){
                    $('#new_mdp').parents('.form-group').addClass('has-error');
                    $('#mdp_check').parents('.form-group').addClass('has-error');
                    isValid = false;
                }
                break;
            case "3":
                if($('#new_email').val() === "" || $('#email_check').val() === ""){
                    highlightEmailInputs();
                    isValid = false;
                }
                break;
            default:
                isValid = false;
                break;
        }
        
        return isValid;
    }
    
    function resetNewUserForm(){
        $('#ajoutUtilisateurForm')[0].reset();
        $('#insertMsg').empty();
    }
    
    // agit lorsque l'utilisateur complète le champ login
    // ajout de classes Bootstrap d'erreur si login indisponible
    $('#new_login').change(function(){
        checkLogin();
    });
    
    // agit lorsque l'utilisateur complète le second champ mot de passe
    // ajout d'une classe Bootstrp d'erreur si les deux champs ne correspondent pas
    $('#mdp_check').change(function(){
        checkMdp();
    });
    
    // agit lorsque l'utilisateur complète le second champ email
    // ajout d'une classe Bootstrp d'erreur si les deux champs ne correspondent pas
    $('#email_check').change(function(){
        checkEmailInsert();
    });
    
    $('#new_email').change(function(){
        if($('#new_email').parents('.form-group').hasClass('has-error')){
            checkEmailInsert();
        }
    });
    
    
    
    // agit lorsque l'utilisateur sélectionne un rôle
    // enlève la classe Bootstrap d'erreur si présente
    $('#new_id_role').change(function(){
        if($('#new_id_role').parents('.form-group').hasClass('has-error')){
            $('#new_id_role').parents('.form-group').removeClass('has-error');
            $('#new_id_role+.help-block').hide();
        }
            
    });
    
    // agit lorsque l'utilisateur coche ou décoche la case newsletter
    $('#new_newsletter').change(function(){
        checkNewsletterAndEmail();
    });
    
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="FORMULAIRE D'INSERTION D'UN NOUVEL UTILISATEUR >>>> vérifications et appels controller">
    // ============================================================
    // ====== FORMULAIRE D'INSERTION D'UN NOUVEL UTILISATEUR ======
    // ============================================================
       
    // agit lorsqu'on clique sur le bouton de validation du formulaire
    $('#ajoutUtilisateurForm #addBtn').click(function(){
        // si aucun rôle n'a été sélectionné, ajout classe Bootstrap d'erreur
        if(checkIdRole()){
            // si les champs requis n'ont pas été complétés, ajout classe Bootstrap d'erreur sur les champs concernés
            if(checkRequirementsByRole() && checkNewsletterAndEmail()){
                // si tout est ok, call ajax pour insérer le nouvel utilisateur dans la bdd (après ultime vérification des champs)
                $.ajax({
                    url:'/application/utilisateur/ajouterUtilisateur',
                    method:'POST',
                    datatype:'text',
                    data:{
                        id_role: $('#new_id_role').val(),
                        nom: $('#new_nom').val(),
                        email: $('#new_email').val(),
                        email_check: $('#email_check').val(),
                        tel: $('#new_tel').val(),
                        newsletter: $('#new_newsletter').is(':checked'),
                        login : $('#new_login').val(),
                        mdp: $('#new_mdp').val(),
                        mdp_check: $('#mdp_check').val()
                    },
                    success: function(data){
                        $('#insertMsg').text(data);
                        window.setTimeout(resetNewUserForm, 4000);
                    },
                    error: function(data){
                        alert('ERREUR');
                    }
                });
            }
        }
    });

//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="FORMULAIRE DE RECHERCHE D'UTILISATEURS >>>> appel controller">

// ====================================================
// ====== FORMULAIRE DE RECHERCHE D'UTILISATEURS ======
// ====================================================
    
    $('#rechercheUtilisateurForm #searchBtn').click(function(){        
        $.ajax({
                    url:'/application/utilisateur/rechercheUtilisateur',
                    method:'POST',
                    datatype:'text',
                    data:{
                        id_role: $('#id_role_search').val(),
                        nom: $('#nom_search').val(),
                        email: $('#email_search').val(),
                        tel: $('#tel_search').val(),
                        newsletter: $('#newsletter_search').is(':checked'),
                        login : $('#login_search').val(),
                        mdp: $('#mdp_search').val()
                    },
                    success: function(data){
                        // on cible le div dans lequel on va insérer le résultat de la requête.
                        var divResultatRecherche = $("#resultatRechercheUtilisateur");
                        // on le vide préalablement de son contenu
                        divResultatRecherche.empty();
                        divResultatRecherche.html(data);
                        $('body, html').animate({scrollTop : ($("#resultatRechercheUtilisateur").offset().top - 70) }, 'slow');
                    },
                    error: function(data){
                        alert('ERREUR');
                    }
                });
    });
// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="EMAILS DES UTILISATEURS INSCRITS A LA NEWSLETTER >>>> appel controller">

// ==============================================================
// ====== EMAILS DES UTILISATEURS INSCRITS A LA NEWSLETTER ======
// ==============================================================
    $('a[href="#getEmailsNewsletter"]').click(function(){
        $.ajax({
                    url:'/application/utilisateur/getEmailsNewsletter',
                    method:'POST',
                    datatype:'text',
                    data:{},
                    success: function(data){
                        var divResultat = $("#getEmailsNewsletter .panel-body p");
                        divResultat.empty().text(data);
                    },
                    error: function(data){
                        alert('ERREUR');
                    }
                });
    });
// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="UPDATE UTILISATEUR >>>> appel controller">

$(document).delegate('.updateFicheUtilisateur', 'click', function(){
    $.ajax({
        url:'/application/utilisateur/afficherFormUpdateUtilisateur',
        method:'POST',
        datatype:'text',
        data:{
            id: $(this).parent().next().val()
        },
        success: function(data){
            // on cible le div dans lequel on va insérer le html obtenu.
            var cible = $("#updateUserModal");
            // on le vide préalablement de son contenu et on insère le html obtenu
            cible.empty();
            cible.html(data);
            
        },
        error: function(data){
            alert('ERREUR');
        }
    });
});

$(document).delegate('#submitUpdateUtilisateur', 'click', function(e){
    $.ajax({
        url: '/application/utilisateur/updateUtilisateur',
        method: 'POST',
        dataType: 'text',
        data:{
            id: $('#idUtilisateurUpdate').val(),
            login: $('#loginUpdate').text(),
            id_role: $('#id_roleUpdate').val(),
            mdp: ($('#mdpUpdate').val() !== "")? $('#mdpUpdate').val() : $('#mdpCheck').val(),
            nom: $('#nomUpdate').val(),
            tel: $('#telUpdate').val(),
            email: $('#emailUpdate').val(),
            newsletter: $('#newsletterUpdate').is(':checked')
        },
        success: function (data){
            $('#updateUtilisateurSuccessMsg').show();
            setTimeout(function(){
                $('#updateUserModal').modal('hide');
                window.location.reload();
            }, 2500);
        },
        error: function(data){
            alert("error");
        }
    }); 
});
//</editor-fold>

});