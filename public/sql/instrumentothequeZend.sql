-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 12 Mars 2017 à 23:30
-- Version du serveur :  5.6.34
-- Version de PHP :  7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `instrumentothequeZend`
--

-- --------------------------------------------------------

--
-- Structure de la table `Categorie`
--

CREATE TABLE `Categorie` (
  `id` int(11) NOT NULL,
  `id_sousfamille` int(11) NOT NULL,
  `legende` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `infos_comp1` text COLLATE utf8_bin,
  `infos_comp2` text COLLATE utf8_bin,
  `video` varchar(600) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Categorie`
--

INSERT INTO `Categorie` (`id`, `id_sousfamille`, `legende`, `nom`, `infos_comp1`, `infos_comp2`, `video`) VALUES
(1, 1, '.A', 'caisses claires', '', '', ''),
(2, 1, '.B', 'grosses caisses', '', '', ''),
(3, 1, '.C', 'toms', '', '', ''),
(4, 1, '.D', 'tambours', '', '', ''),
(5, 1, '.E', 'tambourins', '', '', ''),
(6, 1, '.F', 'timbales', '', '', ''),
(7, 2, '.A', 'cloches', '', '', ''),
(8, 2, '.B', 'maracas', '', '', ''),
(9, 2, '.C', 'bâtons de pluie', '', '', ''),
(10, 2, '.D', 'divers', '', '', ''),
(11, 3, '.A', 'bois', '', '', ''),
(12, 3, '.B', 'métal', '', '', ''),
(13, 4, '.A', 'guitares classiques', '', '', ''),
(14, 4, '.B', 'guitares folk', '', '', ''),
(15, 4, '.C', 'guitares électro-acoustiques', '', '', ''),
(16, 4, '.D', 'guitares électriques', '', '', ''),
(17, 4, '.E', 'minis', '', '', ''),
(18, 4, '.F', 'basses', '', '', ''),
(19, 4, '.G', 'banjos', '', '', ''),
(20, 4, '.H', 'mandolines', '', '', ''),
(21, 4, '.I', 'autres', '', '', ''),
(22, 5, '.A', 'violons', '', '', ''),
(23, 5, '.B', 'altos', '', '', ''),
(24, 5, '.C', 'violoncelles', '', '', ''),
(25, 5, '.D', 'contrebasses', '', '', ''),
(26, 5, '.E', 'accessoires', '', '', ''),
(27, 6, '.A', 'flûtes', 'Une flûte est un instrument de musique à vent dont le son est créé par la vibration d\'un souffle d’air se fendant sur un biseau droit, en encoche ou en anneau. Ce souffle peut être dirigé par un conduit ou par les lèvres de l\'instrumentiste ou provenir d\'une soufflerie mécanique. Le plus souvent de forme tubulaire mais parfois globulaire, en graminée, en bois, en os ou en corne, mais aussi en pierre, en terre cuite, en plastique, en métal (or, argent…), en ivoire et même en cristal, la flûte peut être formée d\'un ou de plusieurs tuyaux, avec ou sans trous, ou posséder une coulisse.', '', ''),
(28, 6, '.B', 'clarinettes', 'La clarinette (du provençal clarin désignant un hautbois1) est un instrument de musique à vent de la famille des bois caractérisée par son anche simple et sa perce quasi cylindrique. Elle a été créée vers 1690 par Johann Christoph Denner (1655-1707) à Nuremberg sur la base d\'un instrument à anche simple plus ancien : le « chalumeau ». La clarinette soprano (en si?) est le modèle le plus commun.\\r\\n<br/><br/>\\r\\nLa perce cylindrique de la clarinette la distingue du hautbois et du saxophone, tous deux à perce conique, et lui confère une aptitude au quintoiement. Son timbre chaud dans le registre grave, peut s\'avérer extrêmement brillant voire perçant dans l\'aigu.\\r\\n<br/><br/>\\r\\nDe tous les instruments à vent de sa famille, la clarinette possède la plus grande tessiture avec trois octaves plus une sixte mineure, soit 45 notes en tout. Elle se décline en une famille d\'instruments presque tous transpositeurs, depuis la clarinette contrebasse jusqu\'à la clarinette sopranino, couvrant ainsi toute l\'étendue d\'un orchestre symphonique. À l\'exception des percussions, la clarinette est l\'instrument qui possède la plus grande famille.\\r\\n<br/><br/>\\r\\nOn utilise la clarinette dans la musique classique e', 'La clarinette en sibémol (mais aussi celles en la, en ut, en ré et mibémol) se présente sous la forme d\'un long tuyau droit. La clarinette est généralement réalisée en bois noble tel que l\'ébène ou le palissandre (au moins pour le corps). Certains modèles, dits d\'études, sont parfois moulés en plastique. Dans les années 1930 le jazz a utilisé des modèles en métal.\\r\\n<br/><br/>\\r\\n\\r\\nEn 1994, des clarinettes en matériau composite ont fait leur apparition. Cette gamme d\'instruments est développée par Buffet Crampon sous l\'appellation Green Line et fabriquée sur la base d\'un matériau constitué de 95 % de poudre d\'ébène et de 5 % de fibre de carbone. Ces clarinettes présentent les avantages du bois sans leurs inconvénients : elles conservent la sonorité de l\'ébène et gagnent en légèreté.\\r\\n<br/><br/>\\r\\n\\r\\nLes clés sont en maillechort (alliage à base de nickel) nickelé, parfois argenté, ou plus rarement doré.\\r\\nphoto : les clefs d\'une clarinette\\r\\nDifférents types de clé sur une clarinette.\\r\\n<br/><br/>\\r\\n\\r\\nPour des raisons pratiques de fabrication et de transport, la clarinette se compose de 5 éléments principaux (de haut en bas) :\\r\\n<ul>\\r\\n<li>le bec, sa ligature et l\'anche fixée sur la partie inf', 'https://www.youtube.com/embed/4JnXC3xxtww'),
(29, 6, '.C', 'saxophones', 'Le saxophone est un instrument de musique à vent appartenant à la famille des bois. Il a été inventé par le Belge Adolphe Sax et breveté à Paris le 21 mars 1846.\\r\\n<br/><br/>\\r\\nIl ne doit pas être confondu avec le saxhorn, de la famille des cuivres, mis au point, lui aussi, par Adolphe Sax. Le saxophone est généralement en laiton, bien qu\'il en existe certains modèles en cuivre, en argent, en plastique ou plaqués en or.', 'Le corps du saxophone est composé de trois parties trouées ou collées réalisées en laiton : le corps conique, le pavillon et la culasse reliant les deux. Les clés (au nombre de 19 à 22 selon les membres de la famille et le modèle) commandent l\'ouverture et la fermeture des trous latéraux percés sur le corps (ou cheminées). L\'extrémité haute du corps est prolongée horizontalement par le bocal (démontable) qui porte le bec (en ébonite, en métal ou en bois), équipé d\'une anche simple attachée avec une ligature.\\r\\n<br/><br/>\\r\\n\\r\\nLe son du saxophone est produit à l\'aide d\'un bec et d\'une anche (en général en roseau, mais peut être aussi en matière synthétique). C\'est la vibration de l\'anche sur la facette du bec qui permet l\'émission du son par mise en vibration de la colonne d\'air contenue dans le corps de l\'instrument.\\r\\n<br/><br/>\\r\\n\\r\\nBien que métallique, le saxophone appartient à la famille des bois de par son mode de production des notes, par la vibration d\'une anche en bois contre le bec. Il est cependant parfois considéré (à tort) comme faisant partie de la section cuivres dans les musiques populaires (telles que le rock, le pop, le rhythm’n’blues, le funk ou le soul) où il est assoc', ''),
(30, 6, '.D', 'hautbois', '', '', ''),
(31, 6, '.E', 'harmonicas', 'L\'harmonica est un instrument de musique à vent et à anche libre fonctionnant sur le même principe que l\'accordéon : des anches (lamelles) métalliques de longueurs différentes, produisent les notes en vibrant au passage de l\'air, aspiré ou soufflé cette fois par la bouche, cette conjonction étant peu fréquente pour un instrument à vent.\\r\\n<br/>\\r\\n<ul>\\r\\nD\'une tessiture normale de trois octaves, il se décline en trois grandes familles :\\r\\n<li>l\'harmonica diatonique simple ;</li>\\r\\n<li>l\'harmonica diatonique double (appelé aussi trémolo) ;</li>\\r\\n<li>l\'harmonica chromatique.</li>\\r\\n</ul>\\r\\n', 'Malgré la connaissance réduite du grand public pour cet instrument, il semblerait que l\'harmonica soit « en quantité » l\'instrument le plus vendu au monde. Si la plupart des gens ont déjà vu un harmonica, il garde un peu toutefois le statut de jouet sans intérêt musical primordial pour certains. Paradoxalement, l\'harmonica semble mieux considéré hors de l\'Europe que sur le vieux continent où il a été créé. Aux États-Unis bien sûr grâce au blues, mais également au Japon et dans bon nombre de pays d\'Asie où des orchestres entiers d\'harmonicas interprètent les œuvres du répertoire classique.\\r\\n<br/><br/>\\r\\n\\r\\nEn outre un harmoniciste diatonique possède souvent plusieurs modèles différents complémentaires en tonalités (jusqu\'à 12 parfois), auxquels s\'ajoutent les harmonicas spéciaux, par exemple ceux de tessiture plus graves ou plus aigues et compte-tenu du fait que contrairement à d\'autres instruments pouvant être conservés à vie moyennant révisions, l\'harmonica s\'abime assez vite surtout chez le débutant et doit être changé. Cependant l\'usure est moindre en utilisant seulement le bas de la colonne d\'air, mais le faible prix de la plupart des modèles incite beaucoup d\'harmonicistes à rac', ''),
(32, 6, '.F', 'accordéons', 'L\'accordéon chromatique est un instrument à vent qui fonctionne par l\'actionnement d\'un soufflet. Contrairement à l\'accordéon diatonique (système bi-sonore), une touche produit la même note en tirant ou en poussant le soufflet (système uni-sonore).\\r\\n<br/><br/>\\r\\nIl est joué aussi bien dans les groupes de musique traditionnelle (balkaniques etc.), que par ceux de musique classique et contemporaine, et dans le jazz.', 'Jeu des mains\\r\\n<br/><br/>\\r\\nLe musicien ouvre et referme le soufflet central, positionné entre les deux parties droite et gauche de l\'instrument, munie chacune d\'un clavier: une partie droite, qui reste statique, et une partie gauche, qui s\'écarte et se rapproche de la partie droite à chaque va-et-vient du soufflet (on parle de « tiré » ou de « poussé » du soufflet). En même temps, l\'instrumentiste appuie sur les touches des claviers de l\'instrument pour décider des notes à produire. L\'air du soufflet passe ainsi dans le mécanisme, et actionne une ou plusieurs anches accordées à la lime et au grattoir. L\'anche au repos possède une courbure qui la porte « au vent » : le réglage de cette courbure a pour but de permettre et faciliter l\'attaque, à toutes les puissances.\\r\\n<br/><br/>\\r\\nL\'accordéon chromatique possède les 12 demi-tons de la gamme chromatique. Une touche enfoncée produira la même note que l\'on tire ou que l\'on pousse le soufflet. Certains ont des boutons, d\'autres des touches de piano. Suivant les modèles, la tessiture peut dépasser 4 ou 5 octaves.', 'https://www.youtube.com/embed/F8jYP9hPrnM'),
(33, 6, '.G', 'divers', '', '', ''),
(34, 7, '.A', 'trompettes', 'La trompette est un instrument de musique à vent de la famille des cuivres clairs. Elle est fabriquée dans un tube de 1,50 m de long comme le cornet. Pour en jouer on utilise des pistons, ainsi que de l\'air (colonne d\'air).', 'La trompette à pistons (pistons de type \"Périnet\") en si\\flat \"plus communément appelée trompette en si bémol\" est celle qui est la plus utilisée aujourd’hui dans la plupart des pays. Mais la trompette à valves rotatives (appelée aussi \"trompette à palettes\") est largement présente en Allemagne et dans les pays de l’est. La trompette en ut est aussi beaucoup utilisée, en particulier en France, dans les orchestres symphoniques et pour certains concerti pour trompette. Elle existe aussi en version à pistons ou à valves rotatives.\\r\\n<br/><br/>\\r\\n\\r\\nÀ cause d’une attaque trop aléatoire avec une trompette normale en si\\flat, certains instrumentistes utilisent la trompette piccolo pour jouer surtout des œuvres baroques dans lesquelles le registre aigu est souvent très utilisé (anciennement appelé clarino). La trompette piccolo ne monte pas plus haut que la trompette normale en si\\flat, elle n\'est pas plus facile à jouer dans le registre aigu, cependant les traits aigus sont plus stables. Elle existe en version à pistons ou à valves rotatives. La plupart du temps, elle est aussi en si\\flat (qui peut être mise en la avec une coulisse additionnelle), parfois en ré.\\r\\nTrompette de poche\\r\\n(\"pocke', 'https://www.youtube.com/embed/CGeZeQ3CmzE'),
(35, 7, '.B', 'trombones', 'Le trombone est un instrument de musique de la famille des vents et de la sous-famille des cuivres. Tout simplement parce qu&rsquo;il faut souffler dedans, et qu&rsquo;il est fait en cuivre (enfin pas vraiment, mais c&rsquo;est parce qu&rsquo;il a évolué).\\r\\n<br/><br/>\\r\\nQue le trombone soit ténor ou basse, son registre est plus grave que celui d\'une trompette. Sa forme allongée courbée comme un s et surtout sa section de tube cylindrique, qui lui donne un son plus brillant, le distinguent des euphoniums ou des saxhorns au registre comparable, au son plus feutré.\\r\\n<br/><br/>\\r\\nIl est utilisé dans de nombreux genres musicaux, de la musique classique au jazz, en passant par la salsa, le ska, le funk ou la musique militaire, et est joué dans les orchestres symphoniques, orchestres d\'harmonie, les fanfares, les big bands, les brass bands, etc.\\r\\n<br/><br/>\\r\\nUn joueur de trombone est appelé tromboniste ou trombone.', '<p>Un trombone est constitué de 3 parties démontables : l&#8217;embouchure, la coulisse et le pavillon. L&#8217;embouchure se place sur les lèvres, c&rsquo;est à l&rsquo;intérieur de celle-ci que le son se forme. La coulisse&#8230; elle coulisse, ce qui permet  de changer de note, plus on l&rsquo;agrandi, plus le son sera grave. Le pavillon sert d&rsquo;amplificateur.</p>\\r\\n<p>L&#8217;embouchure s&#8217;emboite dans la coulisse qui s&#8217;emboite dans le pavillon pour former un trombone.</p>\\r\\n<p>Il y a 5 sortes de trombone :</p>\\r\\n<p>&#8211; Le trombone alto, en mi bémol. Il est principalement utilisé dans la musique orchestrale pour jouer les parties les plus aiguës, car il est plus petit (plus petit = plus haut). Les positions sont différentes des autres trombones.</p>\\r\\n<div id=\"attachment_20\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"wp-image-20 size-medium\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/alto-300x121.jpg\" alt=\"trombone alto\" width=\"300\" height=\"121\" /><p class=\"wp-caption-text\">trombone alto</p></div>\\r\\n<p>&#8211; Le trombone simple, en si bémol. On l&rsquo;utilise principalement dans le jazz ou pour commencer le trombone car i', 'https://www.youtube.com/embed/KlWG5ZpFmeM'),
(36, 7, '.C', 'tubas', '', '', ''),
(37, 7, '.D', 'cors', '', '', ''),
(38, 7, '.E', 'didgeridoo', '', '', ''),
(39, 7, '.F', 'divers', '', '', ''),
(40, 8, '.A', 'amplis guitare', '', '', ''),
(41, 8, '.B', 'amplis basse', '', '', ''),
(42, 9, '.A', 'pédales d\'effet', '', '', ''),
(43, 9, '.B', 'boîtes à rythmes', '', '', ''),
(44, 10, '.A', 'claviers électriques', '', '', ''),
(45, 11, '.A', 'lutrins', '', '', ''),
(46, 11, '.B', 'sangles & harnais', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `Famille`
--

CREATE TABLE `Famille` (
  `id` int(11) NOT NULL,
  `legende` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Famille`
--

INSERT INTO `Famille` (`id`, `legende`, `nom`, `description`) VALUES
(1, '.P', 'percussions', 'Les instruments à percussion, dont on joue en les frappant ou en les agitant, existent depuis les temps les plus reculés de l\'histoire de l\'humanité ; les recherches archéologiques permettent de découvrir soit des vestiges de tels instruments, soit leur représentation graphique. En fait, les deux types essentiels de percussions se retrouvent à toutes les époques : le premier type, comme le tambour, consiste en une peau tendue sur une caisse de résonance, un tronc creusé, une poterie ou une armature métallique ; le second type est formé de lames vibrantes, en bois, métal ou pierre, montées ou non sur un résonateur (xylophone). Il existe des instruments de ce type dans les vestiges de l\'époque néolithique.\\r\\n\\r\\nLes instruments à percussion ont ensuite été liés à l\'avènement des métaux, et en premier lieu du bronze : jeux de cloches, gongs, dont on retrouve trace en Chine aux environs du premier millénaire avant J.-C. (la facture très élaborée des cloches et des gongs chinois implique même une tradition bien plus ancienne). L\'Antiquité grecque et latine offre de nombreux témoignages de l\'utilisation des percussions lors des cérémonies religieuses, au théâtre, au cirque, pour marquer les'),
(2, '.C', 'cordes', 'La famille des cordes doit son nom aux cordes de boyau, en nylon ou en métal tendues sur ces instruments. En les frappant, en les pinçant ou en les frottant, on obtient des sons acoustiquement ou électroniquement amplifiés. '),
(3, '.V', 'vents', 'Un instrument à vent (ou aérophone) est un instrument de musique dont le son est produit grâce aux vibrations d\'une colonne d\'air provoquées par le souffle d\'un instrumentiste (flûte, trompette… ), d\'une soufflerie mécanique (orgue, accordéon) ou d\'une poche d\'air (cornemuse, veuze… ). Les instruments à vent peuvent être fabriqués avec toutes sortes de matières (du bois, du métal, du plastique, du Plexiglas, du cristal, de l\'ivoire ou de l\'os), et certains utilisent des technologies mécaniques, électroniques ou informatiques.\\r\\n\\r\\nLes instruments sont classés par leur méthode de production du son et non par les matériaux qui les composent.\\r\\n\\r\\nDans le cas des Bois, la colonne d\'air est mise en vibration sur un biseau ou par une anche (flûtes, clarinettes, saxophones, hautbois, accordéons...)\\r\\n\\r\\nDans le cas des Cuivres, la colonne d\'air est mise en vibration par les lèvres du musicien, comme la trompette (en métal), le cornet à bouquin et le didgeridoo (en bois) ou l\'olifant (en ivoire).'),
(4, '.E', 'electronique', 'Cette famille regroupe les amplis, les pédales, les effets et les claviers électriques.'),
(5, '.D', 'divers', 'Cette famille regroupe les accessoires de musique, comme les lutrins, les sangles et les harnais.');

-- --------------------------------------------------------

--
-- Structure de la table `Instrument`
--

CREATE TABLE `Instrument` (
  `id` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `ref_mc` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `ref_mf` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `detail_description` text COLLATE utf8_bin,
  `accessoires` text COLLATE utf8_bin,
  `emprunt_ok` tinyint(1) DEFAULT '0',
  `infos_comp1` text COLLATE utf8_bin,
  `infos_comp2` text COLLATE utf8_bin,
  `video` varchar(600) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Instrument`
--

INSERT INTO `Instrument` (`id`, `id_categorie`, `ref_mc`, `ref_mf`, `nom`, `detail_description`, `accessoires`, `emprunt_ok`, `infos_comp1`, `infos_comp2`, `video`) VALUES
(1, 27, 'MC11.V-BOI.A001', '', 'flûte de pan 10 tuyaux', '', '', 1, 'La flûte de Pan est un instrument de musique composé d\'un ensemble de tuyaux sonores assemblés.\\r\\nLa flûte de Pan est un aérophone, le matériau vibrant produisant le son est donc l\'air. Et plus précisément, puisqu\'il s\'agit d\'une flûte, le son est obtenu par la rupture d\'une lame d\'air sur un biseau.', '<p><br></p>', 'https://www.youtube.com/embed/hUTgw_D2xfM'),
(2, 27, 'MC11.V-BOI.A002', '', 'flûte des andes 7 tuyaux', '', '', 1, 'Les multiples sortes de flûtes andines dérivent de trois modèles : la flûte à encoche (qui peut être médiane, inférieure ou supérieure, par rapport à l\'épaisseur du tube), la flûte à bec (tarkas ou pinquillos, courante à l\'époque des Chimus, ce dont témoignent abondamment les vases siffleurs précolombiens), et le siku, technologiquement sans doute la plus ancienne. L\'ingéniosité des habitants des Andes a fait varier ces modèles au point qu\'on en recense (d\'après le musicologue Alejandro Vivanco) près de 115 déclinaisons différentes.', ' En Amérique du sud, on utilise une grande variété de flûtes de Pan. Les espagnols les ont appelé par un terme générique :\\r\\nles zampoñas.', 'https://www.youtube.com/embed/lbWlYz9Zv5c'),
(3, 27, 'MC11.V-BOI.A003', '', 'naï - flûte de pan roumaine', '(courbe)', '', 1, 'Le naï roumain est une \"flûte incurvée à une rangée de tuyaux en ordre décroissant unique\". L’évolution de la flûte originelle, plate, vers un instrument concave provient du fait que le folklore roumain comporte un grand nombre de pièces de virtuosité. Seul un instrument de ce type permet des traits extrêmement rapides. Contrairement à la facture des nombreux autres types de flûtes de Pan à travers le monde, Ie naï roumain possède des tuyaux collés entre-eux (fig A). Autrefois, on utilisait de la cire d’abeille, mais aujourd’hui c’est la colle qui est employée. Les matériaux utilisés pour la fabrication des tuyaux sont le bambou, le roseau et le bois foré.', 'La base des tubes vient se loger dans une embase comportant neuf pièces de bois venant rigidifier l’ensemble. Les quelques éléments que nous venons de décrire sont des facteurs importants pour l’interprétation de pièces demandant une certaine virtuosité. En effet, la crispation du musicien sur certains traits rapides, l’exécution d’un vibrato réalisé en bougeant énergiquement la flûte sont autant d’éléments réclamant une facture instrumentale robuste. Le modèle le plus courant est composé de vingt tuyaux allant du Si 3 au Sol 5. Cependant, l’élargissement des répertoires de cet instrument nécessite aujourd’hui l’addition de tuyaux dans le registre inférieur.', ''),
(4, 27, 'MC11.V-BOI.A004', '', 'flutina en platique, made in london', '', '+ boîte carton désuet', 1, 'Flutina est le nom donné au précurseur français de l\'accordéon diatonique. Ici, il s\'agit bien entendu d\'une petite flûte en plastique comme le montre la photo.', '<p><br></p>', ''),
(5, 27, 'MC11.V-BOI.A005', '', 'flûte double 7 trous (4+3)', 'en bois avec ornements gravés', '', 1, '<p><br></p>', '<p><br></p>', ''),
(6, 27, 'MC11.V-BOI.A006', '', 'flûte en bois à encoche', 'verte, avec ornements (dont un coq)', '', 1, '<p><br></p>', '<p><br></p>', ''),
(7, 27, 'MC11.V-BOI.A007', '', 'flûte en bois brun élégamment ornée', '(peinture rose, vert, bleu foncé, bleu clair, bordeaux, ', '', 1, '<p><br></p>', '<p><br></p>', ''),
(8, 27, 'MC11.V-BOI.A008', '', 'flûte en bois brun ornée', '(peinture, figure anthropomorphe stylisée en incise)', '', 1, '<p><br></p>', '<p><br></p>', ''),
(9, 27, 'MC11.V-BOI.A009', '', '', 'aux couleurs de l\'arc-en-ciel (ou presque) avec dessin de colombe', '', 1, '<p><br></p>', '<p><br></p>', ''),
(10, 27, 'MC11.V-BOI.A010', '', 'flûte en bambou', 'peinture rouge sur le bas', '', 1, '<p><br></p>', '<p><br></p>', ''),
(11, 27, 'MC11.V-BOI.A011', '', 'flûte en bois gravé (Flauto dritto)', '', '', 1, '<p><br></p>', '<p><br></p>', ''),
(12, 27, 'MC11.V-BOI.A012', NULL, 'flûte en bambou', 'avec peinture de couleurs (vert, bleu, rouge, jaune, blanc) et dessins noirs.', '', 1, '', '', ''),
(13, 27, 'MC11.V-BOI.A013', NULL, 'flûte en bois, \"cannée\"', '', '', 1, '', '', ''),
(14, 27, 'MC11.V-BOI.A014', NULL, 'flûte longue', 'à motifs géométriques et abstraits en incise / bois, peint en noir (sauf incises)', '', 1, '', '', ''),
(15, 27, 'MC11.V-BOI.A015', NULL, 'flauto dolce tenore (flûte à bec tenor) \"Aulos\"', '', '+ étui en cuir', 1, '', '', ''),
(16, 27, 'MC11.V-BOI.A016', NULL, 'flûte à bec alto \"Bressan Alto\"', '(noire/brune)', '+ housse brun foncé', 1, '', '', ''),
(17, 27, 'MC11.V-BOI.A017', NULL, 'flûte à bec en bois', 'rouge bordeaux, peinte de motifs floraux (jaune, vert)', '+ boîte de rangement recouverte de tissus vert et bleu', 1, '', '', ''),
(18, 27, 'MC11.V-BOI.A018', '', 'flûte traversière ', '', '+ hard case extérieur noir, intérieur pourpre', 1, 'La flûte traversière est un instrument à vent de la famille des bois. La flûte traversière partage, avec les instruments de la famille des flûtes, la méthode de production du son : l\'air soufflé est mis en vibration par un biseau disposé à l\'embouchure.\\r\\n\\r\\nContrairement à la flûte de Pan, la flûte traversière ne comprend qu\'un seul tuyau.\\r\\n\\r\\nLe terme traversière est lié à la position de jeu de l\'instrument par rapport à la bouche du flûtiste, par opposition à de nombreuses autres flûtes. La flûte « traverse » la bouche, contrairement à la plupart des instruments à vent et à la flûte à bec en particulier (flûte droite).', '\\r\\n		<title></title>\\r\\n	\\r\\n	\\r\\n		<p>\\r\\n			La flûte traversière doit son nom à la façon dont on la tient&nbsp;: sur la droite et horizontalement.</p>\\r\\n		<p>\\r\\n			La grande flûte se compose de trois parties séparables&nbsp;:</p>\\r\\n		<ul>\\r\\n			<li>\\r\\n				Une tête, aussi appelée <a href=\"https://fr.wikipedia.org/wiki/Embouchure_%28musique%29\" title=\"Embouchure (musique)\">embouchure</a>. Elle comprend une plaque percée d\'un trou ovale dans lequel on souffle pour produire le son. L\'extrémité supérieure de l\'embouchure est fermée par un bouchon de bois ou de liège tenu entre deux plaques de métal. L\'accord de l\'instrument est effectué en enfonçant plus ou moins l\'embouchure dans le corps, ce qui a pour effet de modifier la longueur de la colonne d\'air.</li>\\r\\n			<li>\\r\\n				Un corps qui porte des <a class=\"mw-redirect\" href=\"https://fr.wikipedia.org/wiki/Clef_%28organologie%29\" title=\"Clef (organologie)\">clés</a> et des plateaux actionnées par les doigts. Ils servent à boucher les trous afin de produire les diffé</li></ul>', 'https://www.youtube.com/embed/wVWRzO2_r_Q&index=24&list=PLIJAMYdRaoJyikJ6O3rCPOXPuQcgsJqgu'),
(19, 27, 'MC11.V-BOI.A019', NULL, 'flûte à bec contre-alto \"Moeck\"', '', '+ hard case extérieur noir, intérieur vert', 1, '', '', ''),
(20, 27, 'MC11.V-BOI.A020', NULL, 'flûte à bec baryton en bois brun', '', 'sans boîtier, malgré indication contraire de Music Fund / à vérifier', 1, '', '', ''),
(21, 27, 'MC11.V-BOI.A021', NULL, 'flûte à bec alto', 'en bois brun, 3 pièces ', '+ boîtier noir intérieur bleu \"Muziek Hakkert\" + tige métallique pour nettoyer l\'instrument', 1, '', '', ''),
(22, 27, 'MC11.V-BOI.A022', NULL, 'flûte à bec basse \"Moeck\"', '', '+ boîte brune', 1, '', '', ''),
(23, 27, 'MC11.V-BOI.A023', NULL, 'flûte traversière \"Yamaha\"', '', '+ boîtier noir intérieur pourpre + étui en cuir bleu marine, intérieur bleu klein avec anses \"Yamaha\"  + essuie jaune.', 1, '', '', 'https://www.youtube.com/embed/wVWRzO2_r_Q&index=24&list=PLIJAMYdRaoJyikJ6O3rCPOXPuQcgsJqgu'),
(24, 27, 'MC11.V-BOI.A024', NULL, 'petite flûte 6 trous', 'noire en bois, ornée de motifs abstraits en incise.', '', 1, '', '', ''),
(25, 28, 'MC11.V-BOI.B001', '', 'clarinette ', 'noire (n°1162765)', '+ hard case noir \"Bundy\" + 2 paquets de anches neuves', 1, '<p><br></p>', '<p><br></p>', ''),
(26, 28, 'MC11.V-BOI.B002', '', 'clarinette', 'noire / quelle marque?', ' + boîtier noir, intérieur rouge ', 1, '<p><br></p>', '<p><br></p>', ''),
(27, 29, 'MC11.V-BOI.C001', '', 'saxophone ', 'doré', '+ hard case noir + anches de rechange + chiffon de nettoyage + pochette à anche.', 1, '<p><br></p>', '<p><br></p>', ''),
(28, 29, 'MC11.V-BOI.C002', '', 'saxophone alto ', 'doré, gravé (sorte de fleur)', '+ housse \"Alysée\" en tissu bleu + protection bec + sangle', 1, '<p><br></p>', '<p><br></p>', ''),
(29, 29, 'MC11.V-BOI.C003', '', 'saxophone soprano ', '(n°30711356), doré avec gravure fleur', '+ pot de vaseline + protection en toile + chiffon blanc + cordon + housse \"Alysée\"', 1, '<p><br></p>', '<p><br></p>', ''),
(30, 29, 'MC11.V-BOI.C004', NULL, 'saxophone', '', '+ housse noire \"Jupiter\"', 1, '', '', ''),
(31, 31, 'MC11.V-BOI.E001', '', 'petit harmonica ', '', '+ housse brune', 1, '<p><br></p>', '<p><br></p>', ''),
(32, 31, 'MC11.V-BOI.E002', '', 'petit harmonica ', '', '+ boîtier plastique dur gris foncé', 1, '<p><br></p>', '<p><br></p>', ''),
(33, 31, 'MC11.V-BOI.E003', NULL, 'petit harmonica \"Hohner Bravo\" en C', '', '', 1, '', '', ''),
(34, 31, 'MC11.V-BOI.E004', NULL, 'harmonica chromatique \"Hohner Chromonika\"', '(bouton poussoir sur le côté) ', '', 1, '', '', ''),
(35, 31, 'MC11.V-BOI.E005', NULL, 'harmonica \"Star\"', '', '+ boîte en carton', 1, '', '', ''),
(36, 31, 'MC11.V-BOI.E006', NULL, 'petit harmonica \"Hohner Pro Harp\" en B', '', '+ boîtier en plastique dur noir', 1, '', '', ''),
(37, 32, 'MC11.V-BOI.F001', '', 'accordéon chromatique ', 'bordeaux', '', 1, '<p><br></p>', '<p><br></p>', ''),
(38, 32, 'MC11.V-BOI.F002', '', 'accordéon chromatique ', 'gris-vert', '', 1, '<p><br></p>', '<p><br></p>', ''),
(39, 32, 'MC11.V-BOI.F003', NULL, 'accordéon chromatique', 'sans marque, bleu roi', '', 1, '', '', ''),
(40, 33, 'MC11.V-BOI.G001', '', 'mélodica alto ', 'rouge et crème', '', 1, '<p>Le mélodica est un instrument de musique à vent, plus précisément à anche libre. Il s\'apparente dans son mode de fonctionnement à un harmonica, mais à la différence de celui-ci, il comporte un clavier, qui peut avoir une portée de une octave et demie à trois octaves. Le son est obtenu en soufflant dans l\'embouchure de l\'instrument, située sur le côté du clavier. La pression d\'une touche permet alors à l\'air de passer à travers l\'anche correspondante et d\'obtenir une note.</p><p>Le nom mélodica lui-même est déposé par la marque Hohner qui a popularisé le concept. D\'autres marques ont produit le même instrument sous d\'autres noms commerciaux, comme mélodion, pianica ou clavietta. Le mot mélodica est resté comme nom générique pour ce genre d\'instrument.<br><br></p>', '<p>Il y a deux types de mélodicas : ceux à boutons et ceux à clavier.<br></p><ul><li>Les mélodicas à clavier sont les plus fréquents. Ils existent avec des claviers de différentes étendues, jusqu\'à environ 36 touches. La main gauche tient le mélodica par une poignée tandis que la main droite joue sur le clavier. On peut également placer un tuyau spécifique à l\'embouchure du mélodica. Dans ce cas, le mélodica étant posé, l\'utilisateur peut se servir de ses deux mains pour réaliser des accords.</li></ul><ul><li>Les mélodicas à boutons ne sont fabriqués que par Hohner, en Soprano et en Alto. Ils sont plus fins que ceux à clavier et sont conçus pour être joués avec les deux mains : la main droite joue sur les touches diatoniques (les touches blanches du piano) et la main gauche sur les touches chromatiques (les touches noires du piano).</li></ul><p></p>', ''),
(41, 33, 'MC11.V-BOI.G002', '', 'kazoo plastique', 'vert et bleu', '', 1, 'Le kazoo, aussi appelé gazou, est un accessoire qui modifie la voix. Il est constitué d\'un tube fermé par une membrane. En chantonnant dans le tube, le musicien fait vibrer la membrane qui transforme le timbre de la voix en sons nasillards aux accents enfantins. Sa fabrication artisanale est assez aisée.', '<p>On retrouve des objets semblables au kazoo depuis des centaines d\'années en Afrique. Ils étaient utilisés pour déguiser la voix d\'une personne ou pour imiter des animaux (voir appeaux), principalement dans des cérémonies. L\'invention officielle du kazoo est attribuée à l\'afro-américain Alabama Vest, au XIXe siècle, en Georgie. Le premier kazoo a été fabriqué par Thaddeus von Clegg, un horloger allemand de la ville de Macon. La production de kazoos commença en 1852, dans le Georgia State Fair.</p><p>Le kazoo le plus répandu est métallique (fabriqué dans l\'État de New York). Cependant, on peut également se fabriquer artisanalement et facilement son propre kazoo, par exemple avec du papier ciré et un peigne.</p><p>Un kazoo amélioré appelé zobo fut lancé en 1895 par Warren Herbert Frost aux États-Unis. Il connut une très grande vogue durant plusieurs dizaines d\'années avant d\'être oublié. Dans le même pays existèrent trois accessoires similaires : le songophone, le sonophone et le vocophone. Ces accessoires sont les équivalents américains du bigophone français.<br><br></p>', ''),
(42, 34, 'MC11.V-CUI.A001', '', 'trompette ', '(n°C125ML)', '+ hard case brun, intérieur jaune + coulisse et bouton de rechange', 1, '<p><br></p>', '<p><br></p>', 'https://www.youtube.com/embed/CGeZeQ3CmzE'),
(43, 35, 'MC11.V-CUI.B001', '', 'trombone ', '(corps, embouchure, coulisse)', '+ produits d\'entretien + hard case noir', 1, '<p>Le trombone est un instrument de musique de la famille des vents et de la sous-famille des cuivres. Tout simplement parce qu’il faut souffler dedans, et qu’il est fait en cuivre (enfin pas vraiment, mais c’est parce qu’il a évolué).</p><p>Que le trombone soit ténor ou basse, son registre est plus grave que celui d\'une trompette. Sa forme allongée courbée comme un s et surtout sa section de tube cylindrique, qui lui donne un son plus brillant, le distinguent des euphoniums ou des saxhorns au registre comparable, au son plus feutré.</p><p>Il est utilisé dans de nombreux genres musicaux, de la musique classique au jazz, en passant par la salsa, le ska, le funk ou la musique militaire, et est joué dans les orchestres symphoniques, orchestres d\'harmonie, les fanfares, les big bands, les brass bands, etc.</p><p>Un joueur de trombone est appelé tromboniste ou trombone.</p>', '<p>Un trombone est constitué de 3 parties démontables : l’embouchure, la coulisse et le pavillon. L’embouchure se place sur les lèvres, c’est à l’intérieur de celle-ci que le son se forme. La coulisse… elle coulisse, ce qui permet  de changer de note, plus on l’agrandi, plus le son sera grave. Le pavillon sert d’amplificateur.</p><p>L’embouchure s’emboite dans la coulisse qui s’emboite dans le pavillon pour former un trombone.</p><p><br></p><p>Il y a 5 sortes de trombone :</p><p>\\n<br></p><p>– Le trombone alto, en mi bémol. Il est principalement utilisé dans \\nla musique orchestrale pour jouer les parties les plus aiguës, car il \\nest plus petit (plus petit = plus haut). Les positions sont différentes \\ndes autres trombones.</p><p>\\n<br></p><div id=\"attachment_20\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"wp-image-20 size-medium\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/alto-300x121.jpg\" alt=\"trombone alto\" width=\"300\" height=\"121\"><p class=\"wp-caption-text\">trombone alto</p></div><p>\\n<br></p><p>– Le trombone simple, en si bémol. On l’utilise principalement dans le jazz ou pour commencer le trombone car il est plus léger</p><p>\\n<br></p><div id=\"attachment_21\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-21\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/tenor-300x86.jpg\" alt=\"trombone simple\" width=\"300\" height=\"86\"><p class=\"wp-caption-text\">trombone simple</p></div><p>\\n<br></p><p>– Le trombone complet, lui aussi en si bémol. C’est le trombone le \\nplus joué dans al musique classique, il est identique au trombone \\nsimple, mais possède une valve lui permettant d’atteindre la 6ème \\nposition.</p><p>\\n<br></p><div id=\"attachment_22\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-22\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/Holton_Bass_Trombones-300x97.jpg\" alt=\"trombone complet\" width=\"300\" height=\"97\"><p class=\"wp-caption-text\">trombone complet</p></div><p>\\n<br></p><p>– Le trombone basse, lui aussi en si bémol. C’est un trombone ténor \\navec une seconde valve et un pavillon plus gros, permettant une plus \\ngrand aisance dans le grave.</p><p>\\n<br></p><div id=\"attachment_23\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-23\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/bass-300x93.jpg\" alt=\"trombone basse\" width=\"300\" height=\"93\"><p class=\"wp-caption-text\">trombone basse</p></div><p>\\n<br></p><p>– Le trombone contre-basse, en mi bémol, mais bien plus grave que le \\ntrombone alto. Ce trombone est énorme, et rarement joué. Ses positions \\nsont aussi différentes.</p><p>\\n<br></p><div id=\"attachment_24\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-24\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/contrebasse-300x88.jpg\" alt=\"trombone contrebasse\" width=\"300\" height=\"88\"><p class=\"wp-caption-text\">trombone contrebasse</p></div><p><br></p>', 'https://www.youtube.com/embed/KlWG5ZpFmeM'),
(44, 35, 'MC11.V-CUI.B002', '', 'trombone ', 'doré', '+ hard case noir + produits d\'entretien + tiges pour le nettoyage de l\'intérieur de l\'instrument', 1, 'Le trombone est un instrument de musique de la famille des vents et de la sous-famille des cuivres. Tout simplement parce qu’il faut souffler dedans, et qu’il est fait en cuivre (enfin pas vraiment, mais c’est parce qu’il a évolué).<br><br>Que le trombone soit ténor ou basse, son registre est plus grave que celui d\'une trompette. Sa forme allongée courbée comme un s et surtout sa section de tube cylindrique, qui lui donne un son plus brillant, le distinguent des euphoniums ou des saxhorns au registre comparable, au son plus feutré.<br><br>Il est utilisé dans de nombreux genres musicaux, de la musique classique au jazz, en passant par la salsa, le ska, le funk ou la musique militaire, et est joué dans les orchestres symphoniques, orchestres d\'harmonie, les fanfares, les big bands, les brass bands, etc.<br><br>Un joueur de trombone est appelé tromboniste ou trombone.', '<p>Un trombone est constitué de 3 parties démontables : l’embouchure, la coulisse et le pavillon. L’embouchure se place sur les lèvres, c’est à l’intérieur de celle-ci que le son se forme. La coulisse… elle coulisse, ce qui permet  de changer de note, plus on l’agrandi, plus le son sera grave. Le pavillon sert d’amplificateur.</p><br><p>L’embouchure s’emboite dans la coulisse qui s’emboite dans le pavillon pour former un trombone.</p><p><br></p><p>Il y a 5 sortes de trombone :</p><p>\\n<br></p><p>– Le trombone alto, en mi bémol. Il est principalement utilisé dans \\nla musique orchestrale pour jouer les parties les plus aiguës, car il \\nest plus petit (plus petit = plus haut). Les positions sont différentes \\ndes autres trombones.</p><p>\\n<br></p><div id=\"attachment_20\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"wp-image-20 size-medium\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/alto-300x121.jpg\" alt=\"trombone alto\" width=\"300\" height=\"121\"><p class=\"wp-caption-text\">trombone alto</p></div><p>\\n<br></p><p>– Le trombone simple, en si bémol. On l’utilise principalement dans le jazz ou pour commencer le trombone car il est plus léger</p><p>\\n<br></p><div id=\"attachment_21\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-21\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/tenor-300x86.jpg\" alt=\"trombone simple\" width=\"300\" height=\"86\"><p class=\"wp-caption-text\">trombone simple</p></div><p>\\n<br></p><p>– Le trombone complet, lui aussi en si bémol. C’est le trombone le \\nplus joué dans al musique classique, il est identique au trombone \\nsimple, mais possède une valve lui permettant d’atteindre la 6ème \\nposition.</p><p>\\n<br></p><div id=\"attachment_22\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-22\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/Holton_Bass_Trombones-300x97.jpg\" alt=\"trombone complet\" width=\"300\" height=\"97\"><p class=\"wp-caption-text\">trombone complet</p></div><p>\\n<br></p><p>– Le trombone basse, lui aussi en si bémol. C’est un trombone ténor \\navec une seconde valve et un pavillon plus gros, permettant une plus \\ngrand aisance dans le grave.</p><p>\\n<br></p><div id=\"attachment_23\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-23\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/bass-300x93.jpg\" alt=\"trombone basse\" width=\"300\" height=\"93\"><p class=\"wp-caption-text\">trombone basse</p></div><p>\\n<br></p><p>– Le trombone contre-basse, en mi bémol, mais bien plus grave que le \\ntrombone alto. Ce trombone est énorme, et rarement joué. Ses positions \\nsont aussi différentes.</p><p>\\n<br></p><div id=\"attachment_24\" style=\"width: 310px\" class=\"wp-caption alignnone\"><img class=\"size-medium wp-image-24\" src=\"http://apprendre-le-trombone.fr/wp-content/uploads/2014/10/contrebasse-300x88.jpg\" alt=\"trombone contrebasse\" width=\"300\" height=\"88\"><p class=\"wp-caption-text\">trombone contrebasse</p></div><p><br></p>', ''),
(45, 36, 'MC11.V-CUI.C001', '', 'tuba ', 'argenté', '+ hardcase noir', 1, 'Le tuba est un instrument de musique appartenant à la famille des cuivres. Le terme générique « tuba » englobe aujourd\'hui une grande diversité d\'instruments distincts, dont les caractéristiques sont sensiblement différentes en fonction de paramètres liés à une facture instable depuis les débuts.', 'Le tuba est l\'instrument le plus grave de la famille des cuivres, dont la tessiture varie en fonction de la longueur du tube. La sonorité varie en fonction de la géométrie globale, et notamment de la proportion de tube conique ou cylindrique par rapport à la longueur totale.  Pour modifier la hauteur des sons, comme sur les autres cuivres, le musicien (tubiste) fait vibrer ses lèvres en agissant sur divers paramètres : tension de l\'appareil musculaire facial (zygomatique), quantité et vitesse de l\'air expulsé, le tout en coordination avec les différentes combinaisons de doigtés. L\'utilisation des 3, 4, 5, voire 6 pistons ou des systèmes rotatifs à palettes (de 3 à 6) permet de modifier la longueur du tube. Le tuba à six pistons a une tessiture de 4 octaves5.  La tessiture dépend des possibilités de chaque tubiste.', 'https://www.youtube.com/embed/PzH4XAv9ZCQ'),
(46, 37, 'MC11.V-CUI.D001', '', 'cornet ', '(n°642403)', '+ hard case noir, intérieur bleu', 1, 'Littéralement, un cornet est un « petit cor ». Aujourd\'hui enroulé comme une trompette, le cornet fut, de ses origines jusqu\'à l\'apparition des pistons, voire un peu après, enroulé comme un cor. Ce petit cor connut plus un emploi de corne d\'appel que d\'instrument de musique. Il servit en Europe centrale aux postillons transportant la malle du courrier pour prévenir de leur arrivée ou de leur départ. Leur facture et leur tonalité n\'était pas réellement fixée. On en trouvait donc de toutes sortes.\\r\\n\\r\\n', 'Un cornet est constitué d\'une embouchure, d\'un tube, de trois pistons et d\'un pavillon. Le tube est conique, ce qui lui donne un son doux, moins brillant que celui de la trompette.<br><br>Son jeu utilise les mêmes doigtés que ceux de la trompette ou du bugle.<br><br>C\'est un instrument soprano, généralement en si\\flat ou la. Le registre courant s\'étend du fa# grave au contre-ut. Il existe aussi des cornets sopranino en mi\\flat possédant un registre plus élevé.<br><br>Le cornet est parfois employé par les débutants dans l\'apprentissage de la trompette, notamment avec les jeunes enfants, étant réputé plus facile à manipuler, et plus facile à prendre dans de petites mains du fait que le tube est plus replié — l\'instrument est plus ramassé qu\'une trompette bien que la longueur de tuyau soit la même (1,50 m).<br><br><img src=\"http://rouses.net/trumpet/olds62/ambcor.jpg\">', 'https://www.youtube.com/embed/r76BQp1I-Wo'),
(47, 38, 'MC11.V-CUI.E001', '', 'didgeridoo ', '', '+ CD \"Aprender a Tocar Didgeridoo\"', 1, 'Le didgeridoo est un instrument de musique à vent. À l\'origine, cet instrument est joué par les Aborigènes du Nord de l\'Australie, son usage semble très ancien, certains prétendent qu\'il pourrait remonter à l\'âge de la pierre (20 000 ans), d\'après une peinture rupestre ancestrale, représentant un joueur de didgeridoo, analysée au carbone 14. C\'est une trompe en bois, lointaine cousine du cor des Alpes ou du tongqin tibétain.', '<p><br></p>', 'https://www.youtube.com/embed/XiP3LAMCrq0'),
(48, 39, 'MC11.V-CUI.F001', '', 'klaxon', '', '', 1, 'De l’anglais klaxon : le mot a été inventé par son premier fabricant F. W. Lovell, d\'après le mot grec klaz? (« hurler »).', '<p><br></p>', 'https://www.youtube.com/embed/NMhPdjN1APQ'),
(49, 1, 'MC08.P-M.A001', '', 'TEST', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(50, 1, 'MC08.P-M.A002', '', 'TEST', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(51, 1, 'MC08.P-M.A003', '', 'TEST', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(52, 1, 'MC08.P-MBR.A004', '', 'TEST', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(53, 1, 'MC12.P-MBR.A005', '', 'TEST image', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(54, 1, 'MC15.P-MBR.A006', '', 'test image', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(55, 1, 'MC16.P-MBR.A007', '', 'test image', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(56, 1, 'MC09.P-MBR.A008', '', 'test img', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(57, 1, 'MC10.P-MBR.A009', '', 'test imgs', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(58, 1, 'MC08.P-MBR.A010', '', 'ezazeasxq<xq', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(59, 1, 'MC08.P-MBR.A011', '', 'yeopuufgjf', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(60, 1, 'MC08.P-MBR.A012', '', 'test verif', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(62, 1, 'MC08.P-MBR.A013', '', 'ddqsdsqdqd', 'dqsdsqdqsdsqdsqdq', NULL, 1, '<p><br></p>', '<p><br></p>', ''),
(63, 1, 'MC08.P-MBR.A014', '', 'sdqdsqdd', '', NULL, 1, '<p>hjdkshgjqsdk</p><p>dsqdsqd<br></p>', '<p>qsdsqdsqds</p><p><br></p><p>dqssdsqd<br></p>', ''),
(64, 1, 'MC08.P-MBR.A015', '', 'fgsdgshqdfTEST', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(65, 1, 'MC08.P-MBR.A016', '', 'TESTTTTTTT', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(66, 1, 'MC08.P-MBR.A017', '', 'mmmmm', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(67, 1, 'MC08.P-MBR.A018', '', 'dropzone test', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(68, 1, 'MC08.P-MBR.A019', '', 'dropzone 2', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(69, 1, 'MC08.P-MBR.A020', '', 'dropzone3', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(70, 1, 'MC08.P-MBR.A021', '', 'dropzone4', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(71, 1, 'MC08.P-MBR.A022', '', 'TEST DROP', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(72, 1, 'TEST REF', NULL, 'TEST DROPZONE YO', NULL, NULL, NULL, NULL, NULL, NULL),
(73, 1, 'yoyo', NULL, 'yooooo', NULL, NULL, NULL, NULL, NULL, NULL),
(74, 1, 'yesss', NULL, 'yess?', NULL, NULL, NULL, NULL, NULL, NULL),
(75, 1, 'ppp', NULL, 'pppp', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 1, 'qqq', NULL, 'qqq', NULL, NULL, NULL, NULL, NULL, NULL),
(77, 1, 'kkkkkk', NULL, 'kkk', NULL, NULL, NULL, NULL, NULL, NULL),
(78, 1, 'aaa', NULL, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(79, 1, 'hhdd', NULL, 'hhhdddd', NULL, NULL, NULL, NULL, NULL, NULL),
(80, 1, 'dropzonetest', NULL, 'dropzoneFinalTest?', NULL, NULL, NULL, NULL, NULL, NULL),
(81, 1, 'MC08.P-MBR.A032', '', 'GGGGGGGG', '', NULL, 1, '<p>test</p><p>et voilà</p><p><br></p>', '<p><br></p>', ''),
(82, 1, 'MC08.P-MBR.A033', '', 'MMMMMM', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(83, 1, 'MC08.P-MBR.A034', '', 'PPPP', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(84, 1, 'MC08.P-MBR.A035', '', 'FFF', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(85, 1, 'MC08.P-MBR.A036', '', 'SSS', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(86, 1, 'MC08.P-MBR.A037', '', 'KKKKKK', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(87, 1, 'MC08.P-MBR.A038', '', 'LLL', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(88, 1, 'MC08.P-MBR.A039', '', 'DDD', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(89, 1, 'MC08.P-MBR.A040', '', 'MMMMMMMM', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(90, 1, 'MC08.P-MBR.A041', '', 'HJHJHJHJ', '', NULL, 1, '<p>sqdsqdqs</p><p>qsdsqdsqd</p><p><b>dqsdsqdsqd</b><br></p>', '<p>ljkh,ghfsqd</p><p><p><i>klmkdlsqmkdmlkdlsmq</i><br></p></p>', ''),
(91, 1, 'MC08.P-MBR.A042', '', 'QQQ', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(92, 1, 'MC08.P-MBR.A043', '', 'SAFARI', '', NULL, 0, '<p>test</p><p>et oui</p>', '<p><br></p>', ''),
(93, 1, 'MC08.P-MBR.A044', '', 'CHROME', '', NULL, 0, '<p>yopla</p><p>zou</p>', '<p><br></p>', ''),
(94, 1, 'MC08.P-MBR.A045', '', 'ppppppp', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(95, 1, 'MC08.P-MBR.A046', '', 'plouf', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(96, 1, 'MC08.P-MBR.A047', '', 'plouf2', '', NULL, 0, '<p><br></p>', '<p><br></p>', ''),
(97, 1, 'MC08.P-MBR.A048', '', 'qqqqqq', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(98, 1, 'MC08.P-MBR.A049', '', 'sssssss', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(99, 1, 'MC08.P-MBR.A050', '', 'aaaaaa', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(100, 1, 'MC08.P-MBR.A051', '', 'zorriiiiipooopiou', '', 'youhou', 0, '<p>you</p><p><p>hou</p><p>hou&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></p></p>', '<p>yi</p><p>haaa<br></p>', ''),
(101, 1, 'MC08.P-MBR.A052', '', 'YYYY', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(102, 1, 'MC08.P-MBR.A053', '', 'JJJJJJ', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(103, 1, 'MC08.P-MBR.A054', '', 'hhhhhh', '', '', 0, '<p><br></p>', '<p><br></p>', ''),
(104, 1, 'MC08.P-MBR.A055', '', 'qqq', '', 'ssss', 0, '<p>sss</p><p>sss<br></p>', '<p>loreù</p><p>sqdsqdqsd</p><p><br></p>', '');

-- --------------------------------------------------------

--
-- Structure de la table `Modification`
--

CREATE TABLE `Modification` (
  `id` int(11) NOT NULL,
  `id_loggedUser` int(11) NOT NULL,
  `id_famille` int(11) DEFAULT NULL,
  `id_sousfamille` int(11) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `id_instrument` int(11) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `datetime_modif` datetime DEFAULT NULL,
  `type_modif` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Photo`
--

CREATE TABLE `Photo` (
  `id` int(11) NOT NULL,
  `id_instrument` int(11) NOT NULL,
  `nom` varchar(1000) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Photo`
--

INSERT INTO `Photo` (`id`, `id_instrument`, `nom`) VALUES
(1, 1, '1489351690MC11.V-BOI.A001.jpg'),
(2, 2, '1489351711MC11.V-BOI.A002.jpg'),
(3, 3, '1489351732MC11.V-BOI.A003.jpg'),
(4, 4, '1489351805MC11.V-BOI.A004.jpg'),
(5, 5, '1489351829MC11.V-BOI.A005.jpg'),
(6, 6, '1489351889MC11.V-BOI.A006.jpg'),
(7, 7, '1489351911MC11.V-BOI.A007.jpg'),
(8, 8, '1489351933MC11.V-BOI.A008.jpg'),
(9, 9, '1489351955MC11.V-BOI.A009.jpg'),
(10, 10, '1489351980MC11.V-BOI.A010.jpg'),
(11, 11, '1489352002MC11.V-BOI.A011.jpg'),
(12, 45, '1489352062MC11.V-CUI.C001.jpg'),
(13, 46, '1489352099MC11.V-CUI.D001.jpg'),
(14, 46, '1489352126MC11.V-CUI.D001:2.jpg'),
(15, 25, '1489352236MC11.V-BOI.B001.jpg'),
(16, 26, '1489352262MC11.V-BOI.B002.jpg'),
(17, 27, '1489352321MC11.V-BOI.C001.jpg'),
(18, 28, '1489352345MC11.V-BOI.C002.jpg'),
(19, 29, '1489352373MC11.V-BOI.E003.jpg'),
(20, 29, '1489352373MC11.V-BOI.E003:2.jpg'),
(21, 31, '1489352406MC11.V-BOI.E001.jpg'),
(22, 32, '1489352431MC11.V-BOI.E002.jpg'),
(23, 37, '1489352459MC11.V-BOI.F001.jpg'),
(24, 38, '1489352489MC11.V-BOI.F002.jpg'),
(25, 40, '1489352604MC11.V-BOI.G001.jpg'),
(26, 41, '1489352651MC11.V-BOI.G002.jpg'),
(27, 42, '1489352709MC11.V-CUI.A001.jpg'),
(28, 43, '1489352857MC11.V-CUI.B001.jpg'),
(29, 44, '1489352942MC11.V-CUI.B002.jpg'),
(30, 47, '1489353057MC11.V-CUI.E001.jpg'),
(31, 48, '1489353089MC11.V-CUI.F001.jpg'),
(32, 49, '1489353935home_icon.png'),
(33, 18, '1489357406MC11.V-BOI.A018.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `Role`
--

CREATE TABLE `Role` (
  `id` int(11) NOT NULL,
  `intitule` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Role`
--

INSERT INTO `Role` (`id`, `intitule`) VALUES
(1, 'admin'),
(2, 'encodeur'),
(3, 'utilisateur lambda');

-- --------------------------------------------------------

--
-- Structure de la table `SousFamille`
--

CREATE TABLE `SousFamille` (
  `id` int(11) NOT NULL,
  `id_famille` int(11) NOT NULL,
  `legende` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `SousFamille`
--

INSERT INTO `SousFamille` (`id`, `id_famille`, `legende`, `nom`, `description`) VALUES
(1, 1, '-MBR', 'membranophones', 'Un membranophone est un instrument de percussion dont les sons sont produits par la vibration d\'une membrane tendue sur un cadre.\\r\\n\\r\\nLa membrane peut être frappée par une main comme sur un djembé, par un instrument (baguettes, balais, etc.), comme sur la caisse claire. Elle peut aussi entrer en vibration par le frottement d\'une tige solidaire de la peau tendue sur un fût résonnant comme la cuica (tambour à friction). Le tambour par exemple est un membranophone.'),
(2, 1, '-IDP', 'idiophones', 'Un idiophone, ou autophone, est un instrument de musique de la famille des percussions dont le son est produit par le matériau de l\'instrument lui-même, lors d\'un impact produit soit par un accessoire extérieur (comme une baguette), soit par une autre partie de l\'instrument (comme des graines sur un filet qui l\'entoure).'),
(3, 1, '-LML', 'lamellaphones', 'On entend ici tout instrument à lamelles frappées ou pincées (marimba, xylophone, sanza...)'),
(4, 2, '-GTR', 'cordes pincées', 'Un instrument à cordes pincées est un instrument de musique dont les cordes sont pincées, le plus souvent manuellement (à mains nues ou à l\'aide de plectre(s)), ou mécaniquement (clavecin par exemple).\\r\\n\\r\\nCeci inclut par exemple :\\r\\n\\r\\nPour les instruments pincés manuellement, la cithare, les guitares, le banjo, la contrebasse, le pipa, le guqin, le chanza…\\r\\n\\r\\nPour les instruments à cordes pincées mécaniquement : Le clavecin, l\'épinette…\\r\\n'),
(5, 2, '-VLN', 'cordes frottées', 'Les instruments à cordes frottées sont utilisés avec un archet, à l\'exception de quelques instruments comme la vielle à roue, dont les cordes sont frottées par le bord d\'un disque.'),
(6, 3, '-BOI', 'bois', 'Famille d\'instruments de musique à vent, les bois se caractérisent par leur système d\'émission du son constitué soit par un biseau comme les flûtes, soit par la vibration d\'une anche simple comme la clarinette ou double comme le hautbois.\\r\\n\\r\\nSi certains sont en métal comme les saxophones, en cristal comme quelques flûtes traversières, en ivoire comme des hautbois baroques, en céramique comme l\'ocarina ou en plastique comme une partie des flûtes à bec, la grande majorité, encore de nos jours, est fabriquée avec toutes sortes d\'essences de bois, d\'où le nom de la famille. Par contre, les instruments en bois où les lèvres créent la vibration, sont classés dans la famille des cuivres, voir le cornet à bouquin ou le didgeridoo australien.'),
(7, 3, '-CUI', 'cuivres', 'La famille des cuivres regroupe des instruments à vent (également appelés aérophones) où le son est produit par vibration des lèvres dans une embouchure.'),
(8, 4, '-AMP', 'amplis', ''),
(9, 4, '-PFX', 'pédales et effets', ''),
(10, 4, '-CLV', 'claviers électriques', ''),
(11, 5, '-ACC', 'accessoires', 'Ex: lutrins, sangles et harnais.');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nom` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `tel` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT '0',
  `login` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `mdp` varchar(300) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`id`, `id_role`, `nom`, `email`, `tel`, `newsletter`, `login`, `mdp`) VALUES
(2, 1, NULL, NULL, NULL, 0, 'admin', '$2y$12$LdsIC8VvK0L.kgFHhkv.BO.OjlYRtQ.4ZTzt/RkmHltVrg6QVCph2'),
(4, 3, '', 'test@test.be', '', 1, '', ''),
(5, 2, 'Laure Godet', 'godet.laure@gmail.com', '', 1, 'Laure', '$2y$12$RPAj04AFlQMH2pWgKaxK.eDtSUXti/QHsT6TJ6Vdtqf.1KE0P3ktS'),
(6, 2, 'Nasseira Tahtah', 'nass@gmail.com', '', 1, 'Nass', '$2y$12$pKooOPvFx8/BpwTe8UzUfOVU3hBHhDw2RKitHKLT60K5/Y8EOmZy6'),
(7, 3, '', 'levisiteur@gmail.com', '', 1, '', ''),
(8, 1, 'Violette Nys', 'violette.nys@gmail.com', '0486710695', 1, 'Violette', '$2y$12$6JBxJ67l8lSGs8cgAFZcI.8zSX9/fqs.GiD2hYU6pfFBuCey80hTi'),
(9, 2, 'Leal', '', '', 0, 'Leal', '$2y$12$xrTMIw5sU2DCUalQyAEF1eOM5vhUfREaI64/8S2Ro4j8HWN7BUQW.');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Categorie`
--
ALTER TABLE `Categorie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Categorie_SousFamille` (`id_sousfamille`);

--
-- Index pour la table `Famille`
--
ALTER TABLE `Famille`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Instrument`
--
ALTER TABLE `Instrument`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Instrument_Categorie` (`id_categorie`);

--
-- Index pour la table `Modification`
--
ALTER TABLE `Modification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Modification_LoggedUser` (`id_loggedUser`),
  ADD KEY `FK_Modification_Utilisateur` (`id_utilisateur`),
  ADD KEY `FK_Modification_Famille` (`id_famille`),
  ADD KEY `FK_Modification_SousFamille` (`id_sousfamille`),
  ADD KEY `FK_Modification_Categorie` (`id_categorie`),
  ADD KEY `FK_Modification_Instrument` (`id_instrument`);

--
-- Index pour la table `Photo`
--
ALTER TABLE `Photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Photo_Instrument` (`id_instrument`);

--
-- Index pour la table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `SousFamille`
--
ALTER TABLE `SousFamille`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_SousFamille_Famille` (`id_famille`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Utilisateur_Role` (`id_role`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Categorie`
--
ALTER TABLE `Categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `Famille`
--
ALTER TABLE `Famille`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `Instrument`
--
ALTER TABLE `Instrument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT pour la table `Modification`
--
ALTER TABLE `Modification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Photo`
--
ALTER TABLE `Photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `Role`
--
ALTER TABLE `Role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SousFamille`
--
ALTER TABLE `SousFamille`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Categorie`
--
ALTER TABLE `Categorie`
  ADD CONSTRAINT `FK_Categorie_SousFamille` FOREIGN KEY (`id_sousfamille`) REFERENCES `SousFamille` (`id`);

--
-- Contraintes pour la table `Instrument`
--
ALTER TABLE `Instrument`
  ADD CONSTRAINT `FK_Instrument_Categorie` FOREIGN KEY (`id_categorie`) REFERENCES `Categorie` (`id`);

--
-- Contraintes pour la table `Modification`
--
ALTER TABLE `Modification`
  ADD CONSTRAINT `FK_Modification_Categorie` FOREIGN KEY (`id_categorie`) REFERENCES `Categorie` (`id`),
  ADD CONSTRAINT `FK_Modification_Famille` FOREIGN KEY (`id_famille`) REFERENCES `Famille` (`id`),
  ADD CONSTRAINT `FK_Modification_Instrument` FOREIGN KEY (`id_instrument`) REFERENCES `Instrument` (`id`),
  ADD CONSTRAINT `FK_Modification_LoggedUser` FOREIGN KEY (`id_loggedUser`) REFERENCES `Utilisateur` (`id`),
  ADD CONSTRAINT `FK_Modification_SousFamille` FOREIGN KEY (`id_sousfamille`) REFERENCES `SousFamille` (`id`),
  ADD CONSTRAINT `FK_Modification_Utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `Utilisateur` (`id`);

--
-- Contraintes pour la table `Photo`
--
ALTER TABLE `Photo`
  ADD CONSTRAINT `FK_Photo_Instrument` FOREIGN KEY (`id_instrument`) REFERENCES `Instrument` (`id`);

--
-- Contraintes pour la table `SousFamille`
--
ALTER TABLE `SousFamille`
  ADD CONSTRAINT `FK_SousFamille_Famille` FOREIGN KEY (`id_famille`) REFERENCES `Famille` (`id`);

--
-- Contraintes pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD CONSTRAINT `FK_Utilisateur_Role` FOREIGN KEY (`id_role`) REFERENCES `Role` (`id`);
