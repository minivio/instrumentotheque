CREATE TABLE Role(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(150) NOT NULL
);

CREATE TABLE Utilisateur(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_role INT UNSIGNED NOT NULL,
    nom VARCHAR(300) NULL,
    email VARCHAR(300) NULL,
    tel VARCHAR(50) NULL,
    newsletter BOOLEAN DEFAULT false,
    login VARCHAR(30) NULL,
    mdp VARCHAR(300) NULL,
    FOREIGN KEY (id_role) REFERENCES Role(id)
);

CREATE TABLE Famille(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(300) NOT NULL,
    description VARCHAR(1200) NULL
);

CREATE TABLE SousFamille(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_famille INT UNSIGNED NOT NULL,
    nom VARCHAR(300) NOT NULL,
    description VARCHAR(1200) NULL,
    FOREIGN KEY (id_famille) REFERENCES Famille(id)
);

CREATE TABLE Categorie(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_sousfamille INT UNSIGNED NOT NULL,
    nom VARCHAR(300) NOT NULL,
    infos_comp1 VARCHAR(1200) NULL,
    infos_comp2 VARCHAR(1200) NULL,
    video VARCHAR(600) NULL,
    audio VARCHAR(600) NULL,
    FOREIGN KEY(id_sousfamille) REFERENCES SousFamille(id)
);

CREATE TABLE Instrument(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_categorie INT UNSIGNED NOT NULL,
    ref_mc VARCHAR(15) NOT NULL,
    ref_mf VARCHAR(30) NULL,
    nom VARCHAR(300) NOT NULL,
    emprunt_ok BOOLEAN default false,
    infos_comp1 VARCHAR(1200) NULL,
    infos_comp2 VARCHAR(1200) NULL,
    video VARCHAR(600) NULL,
    audio VARCHAR(600) NULL,
    FOREIGN KEY (id_categorie) REFERENCES Categorie(id)
);

CREATE TABLE Modification(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_utilisateur INT UNSIGNED NOT NULL,
    id_famille INT UNSIGNED NULL,
    id_sousfamille INT UNSIGNED NULL,
    id_categorie INT UNSIGNED NULL,
    id_instrument INT UNSIGNED NULL,
    datetime_modif DATETIME DEFAULT CURRENT_TIMESTAMP,
    type_modif VARCHAR(150) NOT NULL,
    description VARCHAR(1200) NOT NULL,
    FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id),
    FOREIGN KEY (id_famille) REFERENCES Famille(id),
    FOREIGN KEY (id_sousfamille) REFERENCES SousFamille(id),
    FOREIGN KEY (id_categorie) REFERENCES Categorie(id),
    FOREIGN KEY (id_instrument) REFERENCES Instrument(id)  
);